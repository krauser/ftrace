/*
** libtrace-platform.h for  in /home/lauren_k/teck2/ftrace/libtrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun Jul  1 20:25:47 2012 jean-baptiste laurent
** Last update Sun Jul  1 20:25:48 2012 jean-baptiste laurent
*/

#ifndef LIBTRACE_PLATFORM_H_
# define LIBTRACE_PLATFORM_H_

# if defined(LIBTRACE_SYSTEM_FREEBSD) \
  || defined(LIBTRACE_SYSTEM_OPENBSD) \
  || defined(LIBTRACE_SYSTEM_NETBSD)
#  define LIBTRACE_SYSTEM_BSD
# endif

# if defined(__amd64__)
#  define LIBTRACE_ARCH_AMD64
# elif defined(__i386__)
#  define LIBTRACE_ARCH_I386
# else
#  error Unsupported architecture
# endif

# if defined(LIBTRACE_SYSTEM_LINUX)
#  include "libtrace-linux.h"
# elif defined(LIBTRACE_SYSTEM_BSD)
#  include "libtrace-bsd.h"
# elif defined(LIBTRACE_SYSTEM_DARWIN)
#  include "libtrace-darwin.h"
# else
#  error "Unsuported platform."
# endif

#endif
