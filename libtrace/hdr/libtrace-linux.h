/*
** libtrace-linux.h for  in /home/lauren_k/teck2/ftrace/libtrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun Jul  1 20:25:49 2012 jean-baptiste laurent
** Last update Sun Jul  1 20:25:49 2012 jean-baptiste laurent
*/

#ifndef LIBTRACE_LINUX_H_
# define LIBTRACE_LINUX_H_

# define LT_TRACEME		PTRACE_TRACEME
# define LT_ATTACH		PTRACE_ATTACH
# define LT_DETACH		PTRACE_DETACH
# define LT_GETREGS		PTRACE_GETREGS
# define LT_PEEKDATA		PTRACE_PEEKDATA
# define LT_SINGLESTEP		PTRACE_SINGLESTEP

/*
** Types of 3rd and 4th arguments of ptrace()
*/
# define LT_PT_T3		void*
# define LT_PT_T4		void*

/*
** Null values for 3rd and 4th arguments of ptrace()
*/
# define LT_PT_N3		NULL
# define LT_PT_N4		NULL

#endif
