/*
** libtrace-darwin.h for  in /home/lauren_k/teck2/ftrace/libtrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun Jul  1 20:25:53 2012 jean-baptiste laurent
** Last update Sun Jul  1 20:25:54 2012 jean-baptiste laurent
*/

#ifndef LIBTRACE_DARWIN_H_
# define LIBTRACE_DARWIN_H_

# define LT_TRACEME		PT_TRACE_ME
# define LT_ATTACH		PT_ATTACH
# define LT_DETACH		PT_DETACH
# define LT_SINGLESTEP		PT_STEP

# define LT_PT_T3		caddr_t
# define LT_PT_T4		int

# define LT_PT_N3		0
# define LT_PT_N4		0

#endif
