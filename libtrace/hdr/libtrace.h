/*
** libtrace.h for  in /home/lauren_k/teck2/ftrace/libtrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun Jul  1 20:25:51 2012 jean-baptiste laurent
** Last update Sun Jul  1 20:25:52 2012 jean-baptiste laurent
*/

#ifndef LIBTRACE_H_
# define LIBTRACE_H_

typedef unsigned long	t_data;

struct			s_regtab
{
  t_data		rip;
  t_data		rax;
  t_data		rbx;
  t_data		rcx;
  t_data		rdx;
  t_data		rbp;
  t_data		rsp;
  t_data		rsi;
  t_data		rdi;
  t_data		r8;
  t_data		r9;
  t_data		r10;
  t_data		r11;
  t_data		r12;
  t_data		r13;
  t_data		r14;
  t_data		r15;
};
typedef struct s_regtab	t_regtab;

int			lt_init(void);
int			lt_traceme(void);
int			lt_attach(pid_t);
int			lt_detach(pid_t);
int			lt_getregs(pid_t, t_regtab *);
t_data			lt_getdata(pid_t, void *);
int			lt_singlestep(pid_t);
const char		*lt_syscallname(t_data);

#endif
