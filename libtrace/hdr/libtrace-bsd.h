/*
** libtrace-bsd.h for  in /home/lauren_k/teck2/ftrace/libtrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun Jul  1 20:25:55 2012 jean-baptiste laurent
** Last update Sun Jul  1 20:25:55 2012 jean-baptiste laurent
*/

#ifndef LIBTRACE_BSD_H_
# define LIBTRACE_BSD_H_

# define LT_TRACEME		PT_TRACE_ME
# define LT_ATTACH		PT_ATTACH
# define LT_DETACH		PT_DETACH
# define LT_GETREGS		PT_GETREGS
# define LT_PEEKDATA		PT_READ_D
# define LT_SINGLESTEP		PT_STEP

# define LT_PT_T3		caddr_t
# define LT_PT_T4		int

# define LT_PT_N3		0
# define LT_PT_N4		0

#endif
