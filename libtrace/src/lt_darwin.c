/*
** lt_darwin.c for  in /home/lauren_k/teck2/ftrace/libtrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun Jul  1 20:26:36 2012 jean-baptiste laurent
** Last update Sun Jul  1 20:26:37 2012 jean-baptiste laurent
*/

#include <sys/types.h>
#include <mach/mach.h>
#include <Security/Authorization.h>
#include "libtrace.h"
#include "libtrace-platform.h"

int			g_has_port;
task_t			g_port;
thread_act_port_array_t	g_threads;
mach_msg_type_number_t	g_th_count;

int			lt_get_darwin_port(pid_t pid)
{
  if (g_has_port != 0)
    return (0);
  if (task_for_pid(mach_task_self(), pid, &g_port) != KERN_SUCCESS)
    return (-1);
  if (task_threads(g_port, &g_threads, &g_th_count) != KERN_SUCCESS)
    return (-1);
  g_has_port = 1;
  return (0);
}

int			lt_darwin_auth(void)
{
  AuthorizationRef	auth;
  OSStatus		status;
  AuthorizationItem	right;
  AuthorizationItem	items[1];
  AuthorizationRights	rights;
  AuthorizationFlags	flags;

  status = AuthorizationCreate(NULL,
			       kAuthorizationEmptyEnvironment,
			       kAuthorizationFlagDefaults,
			       &auth);
  right.name = "system.privilege.taskport";
  right.valueLength = 0;
  right.value = NULL;
  right.flags = 0;
  items[0] = right;
  flags = kAuthorizationFlagInteractionAllowed |
    kAuthorizationFlagExtendRights |
    kAuthorizationFlagPreAuthorize;
  status = AuthorizationCopyRights(auth, &rights,
				   kAuthorizationEmptyEnvironment,
				   flags, NULL);
  if (status != 0)
    return (-1);
  return (0);
}
