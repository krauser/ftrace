/*
** lt_getdata.c for  in /home/lauren_k/teck2/ftrace/libtrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun Jul  1 20:26:32 2012 jean-baptiste laurent
** Last update Sun Jul  1 20:26:33 2012 jean-baptiste laurent
*/

#include <sys/types.h>
#include <sys/ptrace.h>
#include <errno.h>
#include <stdlib.h>
#include "libtrace.h"
#include "libtrace-platform.h"

#ifdef LIBTRACE_SYSTEM_DARWIN

#include <mach/mach.h>

extern int		g_has_port;
extern task_t		g_port;

extern int		lt_get_darwin_port(pid_t);

t_data			lt_getdata(pid_t pid, void *addr)
{
  t_data		buf;
  vm_size_t		dsize;

  errno = 0;
  if (g_has_port == 0)
    {
      if (lt_get_darwin_port(pid) == -1)
	{
	  errno = ESRCH;
	  return (-1);
	}
    }
  dsize = sizeof(t_data);
  if (vm_read_overwrite(g_port, (vm_address_t)addr, sizeof(t_data),
			(vm_address_t)&buf, &dsize) != KERN_SUCCESS)
    {
      errno = EIO;
      return (-1);
    }
  return (buf);
}

#else

t_data			lt_getdata(pid_t pid, void *addr)
{
  errno = 0;
  return (ptrace(LT_PEEKDATA, pid, (LT_PT_T3)addr, LT_PT_N4));
}

#endif
