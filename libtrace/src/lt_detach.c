/*
** lt_detach.c for  in /home/lauren_k/teck2/ftrace/libtrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun Jul  1 20:26:34 2012 jean-baptiste laurent
** Last update Sun Jul  1 20:26:35 2012 jean-baptiste laurent
*/

#include <sys/types.h>
#include <sys/ptrace.h>
#include <stdlib.h>
#include "libtrace.h"
#include "libtrace-platform.h"

int		lt_detach(pid_t pid)
{
  return (ptrace(LT_DETACH, pid, LT_PT_N3, LT_PT_N4));
}
