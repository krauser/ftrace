/*
** lt_attach.c for  in /home/lauren_k/teck2/ftrace/libtrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun Jul  1 20:26:38 2012 jean-baptiste laurent
** Last update Sun Jul  1 20:26:39 2012 jean-baptiste laurent
*/

#include <sys/types.h>
#include <sys/ptrace.h>
#include <stdlib.h>
#include "libtrace.h"
#include "libtrace-platform.h"

int		lt_attach(pid_t pid)
{
  return (ptrace(LT_ATTACH, pid, LT_PT_N3, LT_PT_N4));
}
