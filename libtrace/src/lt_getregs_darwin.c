/*
** lt_getregs_darwin.c for  in /home/lauren_k/teck2/ftrace/libtrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun Jul  1 20:26:23 2012 jean-baptiste laurent
** Last update Sun Jul  1 20:26:23 2012 jean-baptiste laurent
*/

#include <sys/types.h>
#include <mach/mach.h>
#include <errno.h>
#include "libtrace.h"
#include "libtrace-platform.h"

extern int	g_has_port;
extern thread_act_port_array_t	g_threads;
extern mach_msg_type_number_t	g_th_count;
extern int	lt_get_darwin_port(pid_t);

#ifdef LIBTRACE_ARCH_AMD64
# define REG(reg) ts64.__r ## reg
#else
# define REG(reg) ts32.__e ## reg
#endif

static void	cp_regs(x86_thread_state_t *x86_state,
			t_regtab *regs)
{
  memset(regs, 0, sizeof(*regs));
  regs->rip = x86_state->uts.REG(ip);
  regs->rax = x86_state->uts.REG(ax);
  regs->rbx = x86_state->uts.REG(bx);
  regs->rcx = x86_state->uts.REG(cx);
  regs->rdx = x86_state->uts.REG(dx);
  regs->rbp = x86_state->uts.REG(bp);
  regs->rsp = x86_state->uts.REG(sp);
  regs->rsi = x86_state->uts.REG(si);
  regs->rdi = x86_state->uts.REG(di);
#if defined(LIBTRACE_ARCH_AMD64)
  regs->r8 = x86_state->uts.REG(8);
  regs->r9 = x86_state->uts.REG(9);
  regs->r10 = x86_state->uts.REG(10);
  regs->r11 = x86_state->uts.REG(11);
  regs->r12 = x86_state->uts.REG(12);
  regs->r13 = x86_state->uts.REG(13);
  regs->r14 = x86_state->uts.REG(14);
  regs->r15 = x86_state->uts.REG(15);
#endif
}

int		lt_getregs(pid_t pid,
			   t_regtab *regs)
{
  x86_thread_state_t		x86_state;
  mach_msg_type_number_t	sc;

  errno = 0;
  if (g_has_port == 0)
    {
      if (lt_get_darwin_port(pid) == -1)
	{
	  errno = ESRCH;
	  return (-1);
	}
    }
  sc = x86_THREAD_STATE_COUNT;
  if (thread_get_state(g_threads[0], x86_THREAD_STATE,
		       (thread_state_t)&x86_state, &sc)
      != KERN_SUCCESS)
    {
      errno = EIO;
      return (-1);
    }
  cp_regs(&x86_state, regs);
  return (0);
}
