/*
** lt_init.c for  in /home/lauren_k/teck2/ftrace/libtrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun Jul  1 20:26:19 2012 jean-baptiste laurent
** Last update Sun Jul  1 20:26:20 2012 jean-baptiste laurent
*/

#include <sys/types.h>
#include <sys/ptrace.h>
#include "libtrace.h"
#include "libtrace-platform.h"

/*
** TODO:
**   With Darwin, ask authorization to trace system.
*/

#ifdef LIBTRACE_SYSTEM_DARWIN

extern int		g_has_port;
extern int		lt_darwin_auth(void);

int			lt_init(void)
{
  g_has_port = 0;
  return (0);
}

# else

int			lt_init(void)
{
  return (0);
}

#endif
