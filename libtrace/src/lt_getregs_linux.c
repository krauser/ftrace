/*
** lt_getregs_linux.c for  in /home/lauren_k/teck2/ftrace/libtrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun Jul  1 20:26:21 2012 jean-baptiste laurent
** Last update Sun Jul  1 20:26:22 2012 jean-baptiste laurent
*/

#include <sys/types.h>
#include <sys/ptrace.h>
#include <sys/user.h>
#include <string.h>
#include <stdlib.h>
#include "libtrace.h"
#include "libtrace-platform.h"

#ifdef LIBTRACE_ARCH_AMD64
# define RIP			rip
# define RAX			rax
# define RBX			rbx
# define RCX			rcx
# define RDX			rdx
# define RBP			rbp
# define RSP			rsp
# define RSI			rsi
# define RDI			rdi
# define R8			r8
# define R9			r9
# define R10			r10
# define R11			r11
# define R12			r12
# define R13			r13
# define R14			r14
# define R15			r15
#else
# define RIP			eip
# define RAX			eax
# define RBX			ebx
# define RCX			ecx
# define RDX			edx
# define RBP			ebp
# define RSP			esp
# define RSI			esi
# define RDI			edi
#endif

int				lt_getregs(pid_t pid,
					   t_regtab *regs)
{
  struct user_regs_struct	lregs;

  if (ptrace(LT_GETREGS, pid, LT_PT_N3, (LT_PT_T4)&lregs) == -1)
    return (-1);
  memset(regs, 0, sizeof(*regs));
  regs->rip = lregs.RIP;
  regs->rax = lregs.RAX;
  regs->rbx = lregs.RBX;
  regs->rcx = lregs.RCX;
  regs->rdx = lregs.RDX;
  regs->rbp = lregs.RBP;
  regs->rsp = lregs.RSP;
  regs->rsi = lregs.RSI;
  regs->rdi = lregs.RDI;
#if defined(LIBTRACE_ARCH_AMD64)
  regs->r8 = lregs.R8;
  regs->r9 = lregs.R9;
  regs->r10 = lregs.R10;
  regs->r11 = lregs.R11;
  regs->r12 = lregs.R12;
  regs->r13 = lregs.R13;
  regs->r14 = lregs.R14;
  regs->r15 = lregs.R15;
#endif
  return (0);
}
