/*
** lt_getregs_bsd.c for  in /home/lauren_k/teck2/ftrace/libtrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun Jul  1 20:26:30 2012 jean-baptiste laurent
** Last update Sun Jul  1 20:26:31 2012 jean-baptiste laurent
*/

#include <sys/types.h>
#include <sys/ptrace.h>
#include <machine/reg.h>
#include <string.h>
#include "libtrace.h"
#include "libtrace-platform.h"

// TODO : Theses defines change from a BSD to another,
//        please adapt them.

#if defined(LIBTRACE_SYSTEM_NETBSD) && \
  defined(LIBTRACE_ARCH_AMD64)
# define RIP(x)		x.regs[_REG_RIP]
# define RAX(x)		x.regs[_REG_RAX]
# define RBX(x)		x.regs[_REG_RBX]
# define RCX(x)		x.regs[_REG_RCX]
# define RDX(x)		x.regs[_REG_RDX]
# define RBP(x)		x.regs[_REG_RBP]
# define RSP(x)		x.regs[_REG_RSP]
# define RSI(x)		x.regs[_REG_RSI]
# define RDI(x)		x.regs[_REG_RDI]
# define R8(x)		x.regs[_REG_R8]
# define R9(x)		x.regs[_REG_R9]
# define R10(x)		x.regs[_REG_R10]
# define R11(x)		x.regs[_REG_R11]
# define R12(x)		x.regs[_REG_R12]
# define R13(x)		x.regs[_REG_R13]
# define R14(x)		x.regs[_REG_R14]
# define R15(x)		x.regs[_REG_R15]
#elif defined(LIBTRACE_ARCH_AMD64)
# define RIP(x)		x.r_rip
# define RAX(x)		x.r_rax
# define RBX(x)		x.r_rbx
# define RCX(x)		x.r_rcx
# define RDX(x)		x.r_rdx
# define RBP(x)		x.r_rbp
# define RSP(x)		x.r_rsp
# define RSI(x)		x.r_rsi
# define RDI(x)		x.r_rdi
# define R8(x)		x.r_r8
# define R9(x)		x.r_r9
# define R10(x)		x.r_r10
# define R11(x)		x.r_r11
# define R12(x)		x.r_r12
# define R13(x)		x.r_r13
# define R14(x)		x.r_r14
# define R15(x)		x.r_r15
#else
# define RIP(x)		x.r_eip
# define RAX(x)		x.r_eax
# define RBX(x)		x.r_ebx
# define RCX(x)		x.r_ecx
# define RDX(x)		x.r_edx
# define RBP(x)		x.r_ebp
# define RSP(x)		x.r_esp
# define RSI(x)		x.r_esi
# define RDI(x)		x.r_edi
#endif

int			lt_getregs(pid_t pid,
				   t_regtab *regs)
{
  struct reg		lregs;

  memset(&lregs, 0, sizeof(lregs));
  if (ptrace(LT_GETREGS, pid, (LT_PT_T3)&lregs, LT_PT_N4) == -1)
    return (-1);
  regs->rip = RIP(lregs);
  regs->rax = RAX(lregs);
  regs->rbx = RBX(lregs);
  regs->rcx = RCX(lregs);
  regs->rdx = RDX(lregs);
  regs->rbp = RBP(lregs);
  regs->rsp = RBP(lregs);
  regs->rsi = RSI(lregs);
  regs->rdi = RDI(lregs);
#if defined(LIBTRACE_ARCH_AMD64)
  regs->r8 = R8(lregs);
  regs->r9 = R9(lregs);
  regs->r10 = R10(lregs);
  regs->r11 = R11(lregs);
  regs->r12 = R12(lregs);
  regs->r13 = R13(lregs);
  regs->r14 = R14(lregs);
  regs->r15 = R15(lregs);
#endif
  return (0);
}
