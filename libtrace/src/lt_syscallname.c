/*
** lt_syscallname.c for  in /home/lauren_k/teck2/ftrace/libtrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun Jul  1 20:26:15 2012 jean-baptiste laurent
** Last update Sun Jul  1 20:26:16 2012 jean-baptiste laurent
*/

#include <sys/types.h>
#include <stdlib.h>
#include <stdio.h>
#include "libtrace.h"
#include "libtrace-platform.h"

extern const char*	g_syscalls[];
extern const unsigned int	g_syscall_count;

#ifndef LIBTRACE_SYSTEM_DARWIN

const char		*lt_syscallname(t_data call)
{
  if (call >= g_syscall_count)
    return (NULL);
  return (g_syscalls[call]);
}

#else

# define BUF_SIZE	2048

extern const struct
{
  const char            *name;
  const char            **calltab;
  unsigned int          *callcount;
}                       g_darwin_cat[];
extern const unsigned int	g_darwin_cat_count;

const char		*lt_syscallname(t_data call)
{
  unsigned int		category;
  unsigned long		real_call;
  static char		buf[BUF_SIZE + 1];

  category = call & (0xFF << 24);
  real_call = call ^ category;
  category = category >> 24;
  if (category < g_darwin_cat_count && g_darwin_cat[category].calltab)
    snprintf(buf, BUF_SIZE, "%s_%s",
	     g_darwin_cat[category].name,
	     real_call >= *(g_darwin_cat[category].callcount) ? NULL :
	     g_darwin_cat[category].calltab[real_call]);
  else
    snprintf(buf, BUF_SIZE, "%s trap #%ld",
	     category >= g_darwin_cat_count ? "unknown"
	     : g_darwin_cat[category].name,
	     real_call);
  return (buf);
}

#endif
