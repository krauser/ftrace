/*
** lt_getregs.c for  in /home/lauren_k/teck2/ftrace/libtrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun Jul  1 20:26:25 2012 jean-baptiste laurent
** Last update Sun Jul  1 20:26:25 2012 jean-baptiste laurent
*/

#include <sys/types.h>
#include <sys/ptrace.h>
#include "libtrace.h"
#include "libtrace-platform.h"

/* THIS FILE SHOULD NOT BE USED ANYMORE ! */
#error Obsolete file.

int		lt_getregs_linux(pid_t, t_regtab *);
int		lt_getregs_bsd(pid_t, t_regtab *);
int		lt_getregs_darwin(pid_t, t_regtab *);

#if defined(LIBTRACE_SYSTEM_BSD)
#  define GETREGS_FUNC(p, r) lt_getregs_bsd(p, r)
#elif defined(LIBTRACE_SYSTEM_LINUX)
#  define GETREGS_FUNC(p, r) lt_getregs_linux(p, r)
#elif defined(LIBTRACE_SYSTEM_DARWIN)
#  define GETREGS_FUNC(p, r) lt_getregs_darwin(p, r)
#endif

int		lt_getregs(pid_t p, t_regtab *r)
{
  return (GETREGS_FUNC(p, r));
}
