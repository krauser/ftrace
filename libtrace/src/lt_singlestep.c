/*
** lt_singlestep.c for  in /home/lauren_k/teck2/ftrace/libtrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun Jul  1 20:26:17 2012 jean-baptiste laurent
** Last update Sun Jul  1 20:26:18 2012 jean-baptiste laurent
*/

#include <sys/types.h>
#include <sys/ptrace.h>
#include "libtrace.h"
#include "libtrace-platform.h"

int			lt_singlestep(pid_t pid)
{
  return (ptrace(LT_SINGLESTEP, pid, (LT_PT_T3)1, (LT_PT_T4)0));
}
