.include "variables.mk"
.include "variables_bsd.mk"

$(NAME):	$(OBJDIR) $(OBJ)
	$(AR) $(NAME) $(OBJ)

.SUFFIXES:		.o .c

.for i in $(SOURCES)
$(OBJDIR)/${i:.c=.o}:	$(SRCDIR)/${i}
	$(CC) -o ${.TARGET} $(CFLAGS) -c $(SRCDIR)/${i}
.endfor

$(SRCDIR)/gen_syscall.c:
	$(RUBY) mk/syscall.rb -o $(SRCDIR)/gen_syscall.c

$(OBJDIR):
	$(MKDIR) $(OBJDIR)

clean:
	$(RM) $(OBJ)
	$(RM) $(SRCDIR)/gen_syscall.c

fclean:		clean
	$(RM) $(NAME)

re:		clean $(NAME)
