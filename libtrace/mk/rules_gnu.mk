include mk/variables.mk
include mk/variables_gnu.mk

$(NAME):	$(OBJDIR) $(OBJ)
	$(AR) $(NAME) $(OBJ)

all:		$(NAME)

$(OBJDIR)/%.o:	$(SRCDIR)/%.c
	$(CC) -o $@ $(CFLAGS) -c $<

$(OBJDIR):
	$(MKDIR) $(OBJDIR)

$(SRCDIR)/gen_syscall.c:
	$(RUBY) mk/syscall.rb -o $(SRCDIR)/gen_syscall.c

clean:
	$(RM) $(OBJ)
	$(RM) $(SRCDIR)/gen_syscall.c

fclean:		clean
	$(RM) $(NAME)

re:		fclean $(NAME)
