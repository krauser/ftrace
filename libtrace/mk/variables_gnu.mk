SYSTEM		 = ${shell uname | tr '[:lower:]' '[:upper:]' | tr '-' '_'}
ARCH		 = ${shell uname -m | tr '[:lower:]' '[:upper:]' | tr '-' '_'}

ifeq "$(SYSTEM)" "DARWIN"
SOURCES		+= lt_getregs_darwin.c \
		   lt_darwin.c \
		   mach_traps.c
CFLAGS		+= -arch i386 -arch x86_64
else
ifeq "$(SYSTEM)" "LINUX"
SOURCES		+= lt_getregs_linux.c
else # BSD
SOURCES		+= lt_getregs_bsd.c
endif
endif

SRC		 = ${addprefix $(SRCDIR)/,$(SOURCES)}
OBJ		 = ${addprefix $(OBJDIR)/,$(SOURCES:.c=.o)}

CFLAGS		+= -DLIBTRACE_SYSTEM_$(SYSTEM) -I$(HDRDIR)
