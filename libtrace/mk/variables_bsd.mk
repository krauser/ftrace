SYSTEM		!= uname | tr '[:lower:]' '[:upper:]' | tr '-' '_'
ARCH		!= uname -m | tr '[:lower:]' '[:upper:]' | tr '-' '_'

.if $(SYSTEM) == "DARWIN"
SOURCES		+= lt_getregs_darwin.c \
		   lt_darwin.c \
		   mach_traps.c
CFLAGS		+= -arch i386 -arch x86_64
.elif $(SYSTEM) == "LINUX"
SOURCES		+= lt_getregs_linux.c
.else # BSD
SOURCES		+= lt_getregs_bsd.c
.endif

SRC		 = $(SOURCES:S/^/$(SRCDIR)\//)
OBJ		 = $(SOURCES:S/^/$(OBJDIR)\//:S/.c/.o/)

CFLAGS		+= -DLIBTRACE_SYSTEM_$(SYSTEM) -I$(HDRDIR)

