#!/usr/bin/env ruby

outfile = nil

i = 0
while i < ARGV.count
  case ARGV[i]
  when "-o"
    outfile = ARGV[i + 1]
    i += 2
  else
    $stderr.puts "Unknown argument #{ARGV[i]}"
    exit 2
  end
end

if outfile
  f = File.open(outfile, "w")
  unless f
    $stderr.puts "Unable to open #{outfile}"
    exit 3
  end
  $stdout.reopen(f)
end

Sysspec = Struct.new(:header, :prefix)

SYSNAME = `uname`.strip + "-" + `uname -m`.strip
SYSSPECS = {
  "Darwin-x86_64" => Sysspec.new("/usr/include/sys/syscall.h", "SYS_"),
  "Darwin-i386" => Sysspec.new("/usr/include/sys/syscall.h", "SYS_"),
  "Linux-x86_64" => Sysspec.new("/usr/include/asm/unistd_64.h", "__NR_"),
  "Linux-i686" => Sysspec.new("/usr/include/asm/unistd_32.h", "__NR_"),
  "FreeBSD-amd64" => Sysspec.new("/usr/include/sys/syscall.h", "SYS_"),
  "FreeBSD-i386" => Sysspec.new("/usr/include/sys/syscall.h", "SYS_"),
  "OpenBSD-amd64" => Sysspec.new("/usr/include/sys/syscall.h", "SYS_"),
  "OpenBSD-i386" => Sysspec.new("/usr/include/sys/syscall.h", "SYS_"),
  "NetBSD-amd64" => Sysspec.new("/usr/include/sys/syscall.h", "SYS_"),
  "NetBSD-i386" => Sysspec.new("/usr/include/sys/syscall.h", "SYS_")
}

unless SYSSPECS.has_key?(SYSNAME)
  $stderr.puts "Your system (#{SYSNAME}) is not supported."
  exit 1
end

SYSSPEC = SYSSPECS[SYSNAME]

puts "#include <stdlib.h>"
puts
puts "const char\t*g_syscalls[] = {"

syscall_tab = []

File.open(SYSSPEC.header, "r") do |f|
  f.each_line do |l|
    l.strip!
    if l =~ /^#define\s+#{SYSSPEC.prefix}(\S+)\s+(\d+)$/
      val = $~[2].to_i
      name = $~[1]

      unless [ "MAXSYSCALL",
               "NSYSENT" ].include?(name)
        syscall_tab[$~[2].to_i] = $~[1]
      end
    end
  end
  
  syscall_tab -= [ "MAXSYSCALL", "NSYSENT" ]
  syscall_tab.each_with_index do |syscall, num|
    if syscall == nil
      puts "  NULL,"
    else
      puts "  \"#{syscall}\","
    end
  end
end

puts "};"
puts ""
puts "const unsigned int\tg_syscall_count = #{syscall_tab.count};"

exit 0
