NAME		 = libtrace.a

CC		 = gcc
CFLAGS		+= -Wall -Wextra -Werror -Wfatal-errors
AR		 = ar rcs
RM		 = rm -f
MKDIR		 = mkdir -p
RUBY		 = ruby

SRCDIR		 = src
OBJDIR		 = build
HDRDIR		 = hdr

SOURCES		 = lt_attach.c \
		   lt_detach.c \
		   lt_getdata.c \
		   lt_init.c \
		   lt_singlestep.c \
		   lt_traceme.c \
		   lt_syscallname.c \
		   gen_syscall.c
