MAKEDIR		 = make -C
MAKEDIR!= sh detect-bsd-make.sh

all:		ftrace

libsymbol:	FORCE
	$(MAKEDIR) libsymbol

libtrace:	FORCE
	$(MAKEDIR) libtrace

ftrace:		libsymbol libtrace FORCE
	$(MAKEDIR) ftrace

clean:
	$(MAKEDIR) libsymbol clean
	$(MAKEDIR) libtrace clean
	$(MAKEDIR) ftrace clean

fclean:
	$(MAKEDIR) libsymbol fclean
	$(MAKEDIR) libtrace fclean
	$(MAKEDIR) ftrace fclean

re:
	$(MAKEDIR) libsymbol fclean
	$(MAKEDIR) libtrace fclean
	$(MAKEDIR) ftrace fclean
	$(MAKEDIR) libsymbol
	$(MAKEDIR) libtrace
	$(MAKEDIR) ftrace


FORCE:

.PHONY:		all clean fclean re FORCE
