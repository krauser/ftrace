#ifndef LIBSYMBOL_H_
# define LIBSYMBOL_H_

# if defined(__amd64__)
#  define LIBSYMBOL_ARCH_AMD64
# elif defined(__i386__)
#  define LIBSYMBOL_ARCH_I386
# elif defined(__ppc__)
#  define LIBSYMBOL_ARCH_PPC
# else
#  error Unsupported architecture
# endif

struct		s_symbol
{
  void		*address;
  char		*name;
};
typedef struct s_symbol t_symbol;

t_symbol	*get_symbols(const char *);

#endif
