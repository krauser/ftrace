#ifndef LIBSYMBOL_MACHO_H_
# define LIBSYMBOL_MACHO_H_

struct		s_memfile
{
  int		fd;
  size_t	size;
  void		*ptr;
};
typedef struct s_memfile t_memfile;

int		map_file(const char *, t_memfile *);
int		unmap_file(t_memfile *);

# define LS_MACH_BINARY		1
# define LS_MACH_FAT		2
# define LS_MACH_INVALID	-1

int		file_type(t_memfile *);

typedef unsigned short t_machtype;

/*
** mach_header and mach_header64 differs from only one field, reserved.
** we can generalize and use one global structure for both.
*/

struct		s_machbin
{
  t_machtype	type;
  struct mach_header	*head;
  struct load_command	*fcmd;
  unsigned int		ncmd;
};
typedef struct s_machbin t_machbin;

int		read_bin(t_memfile *, t_machbin *);

struct load_command	*next_command(t_machbin *,
				      struct load_command *);

# define LS_CPU_X32		(1 << 0)
# define LS_CPU_X64		(1 << 1)

# define LS_ENDIAN_BIG		(1 << 0)
# define LS_ENDIAN_LITTLE	(1 << 1)

# define LS_MKTYPE(cpu, endian)	(((cpu) << 8) + (endian))
# define LS_TYPE_CPU(x)		(((x) & 0xff00) >> 8)
# define LS_TYPE_ENDIAN(x)	((x) & 0x00ff)

struct		s_machfat
{
  unsigned int	arch_count;
  struct fat_arch	*archs;
};
typedef struct s_machfat t_machfat;

int		read_fat(t_memfile *, t_machfat *);

typedef unsigned long long t_ullint;

unsigned int	ls_swap(unsigned int);
t_ullint	ls_swapll(t_ullint);
unsigned int	ls_bintocur(t_machtype srct,
			    unsigned int v);
t_ullint	ls_bintocur_ll(t_machtype srct,
			       t_ullint v);

# if BYTE_ORDER == LITTLE_ENDIAN
#  define LS_BTOC(x)	ls_swap(x)
#  define LS_LTOC(x)	(x)
#  define LS_CTOB(x)	ls_swap(x)
#  define LS_CTOL(x)	(x)
#  define LS_BTOC_LL(x)	ls_swapll(x)
#  define LS_LTOC_LL(x)	(x)
#  define LS_CTOB_LL(x)	ls_swapll(x)
#  define LS_CTOL_LL(x)	(x)
# else
#  define LS_BTOC(x)	(x)
#  define LS_LTOC(x)	ls_swap(x)
#  define LS_CTOB(x)	(x)
#  define LS_CTOL(x)	ls_swap(x)
#  define LS_BTOC_LL(x)	(x)
#  define LS_LTOC_LL(x)	ls_swapll(x)
#  define LS_CTOB_LL(x)	(x)
#  define LS_CTOL_LL(x)	ls_swapll(x)
#endif

const char	*which_bin(const char *);

int		check_platform(t_machbin *bin);
int		find_arch(t_memfile *, t_machfat *, t_memfile *);
t_symbol	*get_bin_checked(t_machbin *);

#endif
