#ifndef		__LIBSYMBOL_ELF_H__
# define	__LIBSYMBOL_ELF_H__

struct		s_memfile
{
  int		fd;
  size_t	size;
  void		*ptr;
};
typedef struct s_memfile t_memfile;

int		map_file(const char *, t_memfile *);
int		unmap_file(t_memfile *);

struct		s_elffile
{
  int		arch;
};
typedef struct s_elffile t_elffile;

struct		s_elffile32
{
  Elf32_Ehdr	*ehdr;
  Elf32_Shdr	*sect;
  unsigned int	sect_count;
  Elf32_Sym	*symbol;
  unsigned int	sym_count;
  Elf32_Rel	*rel;
  unsigned int	rel_count;
  Elf32_Rela	*rela;
  unsigned int	rela_count;
  Elf32_Sym	*dynsym;
  char		*dynstr;
  char		*symnames;
  char		*sectnames;
  char		*plt;
};
typedef struct s_elffile32 t_elffile32;

struct		s_elffile64
{
  Elf64_Ehdr	*ehdr;
  Elf64_Shdr	*sect;
  unsigned int	sect_count;
  Elf64_Sym	*symbol;
  unsigned int	sym_count;
  Elf64_Rel	*rel;
  unsigned int	rel_count;
  Elf64_Rela	*rela;
  unsigned int	rela_count;
  Elf64_Sym	*dynsym;
  char		*dynstr;
  char		*symnames;
  char		*sectnames;
  char		*plt;
};
typedef struct s_elffile64 t_elffile64;

#define LS_ARCH_X86	(1 << 0)
#define LS_ARCH_X64	(1 << 1)

int		read_elf(t_memfile *, t_elffile *);
int		read_elf32(t_memfile *, t_elffile32 *);
int		read_elf64(t_memfile *, t_elffile64 *);

#endif		/* !__LIBSYMBOL_ELF_H__ */
