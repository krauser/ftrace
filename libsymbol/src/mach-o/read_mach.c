#include <sys/types.h>
#include <mach-o/loader.h>
#include "libsymbol.h"
#include "libsymbol-macho.h"

int		read_mach(t_memfile *mem,
			  t_machbin *mach)
{
  mach->head = (struct mach_header *)mem->ptr;
  mach->fcmd = (struct load_command *)(mem->ptr
				       + (LS_TYPE_CPU(mach->type) == LS_CPU_X64 ?
					  sizeof(struct mach_header_64) :
					  sizeof(struct mach_header)));
  mach->ncmd = ls_bintocur(mach->type, mach->head->ncmds);
  return (0);
}
