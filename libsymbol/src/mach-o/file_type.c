#include <sys/types.h>
#include <mach-o/loader.h>
#include <mach-o/fat.h>
#include "libsymbol.h"
#include "libsymbol-macho.h"

int		file_type(t_memfile *mptr)
{
  uint32_t	magic;

  if (mptr->size < sizeof(uint32_t))
    return (LS_MACH_INVALID);
  magic = *((uint32_t*)mptr->ptr);
  if (magic == MH_MAGIC || magic == MH_CIGAM
      || magic == MH_MAGIC_64 || magic == MH_CIGAM_64)
    return (LS_MACH_BINARY);
  if (magic == FAT_MAGIC || magic == FAT_CIGAM)
    return (LS_MACH_FAT);
  return (LS_MACH_INVALID);
}
