#include <sys/types.h>
#include <mach-o/loader.h>
#include <stdlib.h>
#include "libsymbol.h"
#include "libsymbol-macho.h"

struct load_command	*next_command(t_machbin *mach,
				      struct load_command *cmd)
{
  size_t		off;

  if (cmd == NULL)
    return (mach->fcmd);
  off = (size_t)cmd - (size_t)mach->fcmd;
  off += ls_bintocur(mach->type, cmd->cmdsize);
  if (off >= ls_bintocur(mach->type, mach->head->sizeofcmds))
    return (NULL);
  return ((struct load_command *)(off + (size_t)mach->fcmd));
}

