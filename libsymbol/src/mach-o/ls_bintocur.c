#include <sys/types.h>
#include "libsymbol.h"
#include "libsymbol-macho.h"

unsigned int	ls_bintocur(t_machtype srct,
			    unsigned int v)
{
  return (LS_TYPE_ENDIAN(srct) == LS_ENDIAN_BIG ?
	  LS_BTOC(v) : LS_LTOC(v));
}

t_ullint	ls_bintocur_ll(t_machtype srct,
			       t_ullint v)
{
  return (LS_TYPE_ENDIAN(srct) == LS_ENDIAN_BIG ?
	  LS_BTOC_LL(v) : LS_LTOC_LL(v));
}
