#include <sys/types.h>
#include <mach-o/fat.h>
#include "libsymbol.h"
#include "libsymbol-macho.h"

#ifdef LIBSYMBOL_ARCH_AMD64
# define LS_CURRENT_ARCH	LS_CPU_X64
#else
# define LS_CURRENT_ARCH	LS_CPU_X32
#endif

#if BYTE_ORDER == LITTLE_ENDIAN
# define LS_CURRENT_ENDIAN	LS_ENDIAN_LITTLE
#else
# define LS_CURRENT_ENDIAN	LS_ENDIAN_BIG
#endif

int		check_platform(t_machbin *bin)
{
  if (bin->type == LS_MKTYPE(LS_CURRENT_ARCH, LS_CURRENT_ENDIAN))
    return (0);
  return (-1);
}

int		find_arch(t_memfile *mem,
			  t_machfat *fat,
			  t_memfile *bin)
{
  unsigned int	i;
  t_machbin	fake;

  i = 0;
  while (i < fat->arch_count)
    {
      bin->size = LS_BTOC(fat->archs[i].size);
      bin->ptr = (void*)((size_t)mem->ptr + LS_BTOC(fat->archs[i].offset));
      if (read_bin(bin, &fake) == -1)
	return (-1);
      if (check_platform(&fake) == 0)
	return (0);
      ++i;
    }
  return (-1);
}
