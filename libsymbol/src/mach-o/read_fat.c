#include <sys/types.h>
#include <mach-o/fat.h>
#include "libsymbol.h"
#include "libsymbol-macho.h"

int			read_fat(t_memfile *mem, t_machfat *fat)
{
  struct fat_header	*fath;

  fath = (struct fat_header *)mem->ptr;
  fat->arch_count = LS_CTOB(fath->nfat_arch);
  fat->archs = (struct fat_arch*)(fath + 1);
  return (0);
}

