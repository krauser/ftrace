#include <sys/types.h>
#include <mach-o/loader.h>
#include <mach-o/nlist.h>
#include <stdlib.h>
#include <strings.h>
#include <stdio.h>
#include "libsymbol.h"
#include "libsymbol-macho.h"

static t_symbol		*handle_symtab(t_machbin *bin,
				       struct symtab_command *cmd)
{
  t_symbol		*tab;
  struct nlist_64	*sym;
  size_t		symcnt;
  size_t		i;
  char			*symtab;
  size_t		addr;
  uint32_t		off;

  symcnt = ls_bintocur(bin->type, cmd->nsyms);
  if (!(tab = malloc(sizeof(tab) * (symcnt + 1))))
    return (NULL);
  i = 0;
  sym = (struct nlist_64*)((size_t)bin->head + ls_bintocur(bin->type,
							   cmd->symoff));
  symtab = (char *)((size_t)bin->head + ls_bintocur(bin->type,
						    cmd->stroff));
  while (i < symcnt)
    {
      addr = (LS_TYPE_CPU(bin->type) == LS_CPU_X64 ?
	      ls_bintocur_ll(bin->type, sym->n_value) :
	      ls_bintocur(bin->type, *((uint32_t*)(&sym->n_value))));
      off = sym->n_un.n_strx;
      tab[i].address = (void*)addr;
      tab[i].name = (char *)((size_t)symtab + off);
      sym = (LS_TYPE_CPU(bin->type) == LS_CPU_X64 ? sym + 1 :
	     (struct nlist_64*)((struct nlist*)sym + 1));
      ++i;
    }
  tab[i].address = (void*)-1;
  tab[i].name = (void*)-1;
  return (tab);
}

t_symbol		*get_bin_checked(t_machbin *bin)
{
  struct load_command	*lcmd;

  lcmd = bin->fcmd;
  while (lcmd)
    {
      if (ls_bintocur(bin->type, lcmd->cmd) == LC_SYMTAB)
	return (handle_symtab(bin, (struct symtab_command *)lcmd));
      lcmd = next_command(bin, lcmd);
    }
  return (NULL);
}
