#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

#define		WHICH_PATH	"PATH"
#define		WHICH_PATH_SEP	':'
#define		WHICH_LEN	4096

static char	*which_build(const char *pos,
			     int n,
			     const char *bin,
			     char *buf)
{
  int		size;

  size = n;
  if (pos[n - 1] != '/')
    size++;
  size += strlen(bin);
  strncpy(buf, pos, n);
  if (pos[n - 1] != '/')
    {
      buf[n] = '/';
      strcpy(buf + n + 1, bin);
    }
  else
    strcpy(buf + n, bin);
  buf[size] = 0;
  return (buf);
}

static int	which_check(const char *path)
{
  if (access(path, X_OK) == -1)
    return (-1);
  return (0);
}

const char	*which_bin(const char *bin)
{
  const char	*cur;
  const char	*next;
  static char	buf[WHICH_LEN];

  cur = getenv(WHICH_PATH);
  while (*cur)
    {
      next = strchr(cur, WHICH_PATH_SEP);
      if (!next)
	next = cur + strlen(cur);
      which_build(cur, next - cur, bin, buf);
      if (*buf)
	{
	  if (which_check(buf) == 0)
	    return (buf);
	}
      cur = next;
      if (*cur == ':')
	cur++;
    }
  return (0);
}
