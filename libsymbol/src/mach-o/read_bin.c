#include <sys/types.h>
#include <mach-o/loader.h>
#include "libsymbol.h"
#include "libsymbol-macho.h"

extern int	read_mach(t_memfile *,
			  t_machbin *);

int		read_bin(t_memfile *file,
			 t_machbin *bin)
{
  int		cpu;
  int		endian;
  uint32_t	magic; 

  if (file->size < sizeof(struct mach_header))
    return (-1);
  magic = *(uint32_t*)(file->ptr);
  if (magic == MH_MAGIC || magic == MH_MAGIC_64)
    endian = BYTE_ORDER == BIG_ENDIAN ? LS_ENDIAN_BIG : LS_ENDIAN_LITTLE;
  else if (magic == MH_CIGAM || magic == MH_CIGAM_64)
    endian = BYTE_ORDER == BIG_ENDIAN ? LS_ENDIAN_LITTLE : LS_ENDIAN_BIG;
  else
    return (-1);
  if (magic == MH_MAGIC || magic == MH_CIGAM)
    cpu = LS_CPU_X32;
  else if (magic == MH_MAGIC_64 || magic == MH_CIGAM_64)
    cpu = LS_CPU_X64;
  else
    return (-1);
  bin->type = LS_MKTYPE(cpu, endian);
  return (read_mach(file, bin));
}
