#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "libsymbol.h"
#include "libsymbol-macho.h"

static t_symbol		*get_from_bin(t_memfile *mem)
{
  t_machbin		bin;

  if (read_bin(mem, &bin) == -1)
    {
      fprintf(stderr, "Unable to read Mach-O info\n");
      return (NULL);
    }
  if (check_platform(&bin) == -1)
    {
      fprintf(stderr, "Platform mismatch. (Tip: arch(1) et lipo(1) may "
	      "help you)\n");
      return (NULL);
    }
  return (get_bin_checked(&bin));
}

static t_symbol		*get_from_fat(t_memfile *mem)
{
  t_machfat		fat;
  t_memfile		bin;

  if (read_fat(mem, &fat) == -1)
    {
      fprintf(stderr, "Unable to read fat Mach-O info\n");
      return (NULL);
    }
  if (find_arch(mem, &fat, &bin) == -1)
    {
      fprintf(stderr, "This file contains no valid architecture.\n");
      return (NULL);
    }
  return (get_from_bin(&bin));
}

t_symbol		*get_symbols(const char *bin)
{
  const char		*rname;
  t_memfile		memfile;
  int			ftype;
  t_symbol		*ret;

  if (!(rname = (strchr(bin, '/') ? bin : which_bin(bin))))
    {
      fprintf(stderr, "Unable to find %s\n", bin);
      return (NULL);
    }
  if (map_file(rname, &memfile) == -1)
    {
      fprintf(stderr, "Unable to map file\n");
      return (NULL);
    }
  ftype = file_type(&memfile);
  ret = NULL;
  if (ftype == LS_MACH_BINARY)
    ret = get_from_bin(&memfile);
  else if (ftype == LS_MACH_FAT)
    ret = get_from_fat(&memfile);
  else
    fprintf(stderr, "File is not a Mach-O executable.\n");
  unmap_file(&memfile);
  return (ret);
}
