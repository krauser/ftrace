#include <fcntl.h>
#include <sys/mman.h>
#include <unistd.h>
#include "libsymbol.h"
#include "libsymbol-macho.h"

int		unmap_file(t_memfile *stru)
{
  return (0);
  if (munmap(stru->ptr, stru->size) + close(stru->fd) != 0)
    return (-1);
  return (0);
}
