#include <sys/types.h>
#include "libsymbol.h"
#include "libsymbol-macho.h"

union			u_tab
{
  unsigned int		n;
  char			c[sizeof(unsigned int)];
};

union			u_tabll
{
  t_ullint		n;
  char			c[sizeof(t_ullint)];
};

unsigned int		ls_swap(unsigned int val)
{
  unsigned int		i;
  union u_tab		src;
  union u_tab		dst;

  i = 0;
  src.n = val;
  dst.n = 0;
  while (i < sizeof(unsigned int))
    {
      dst.c[i] = src.c[sizeof(unsigned int) - i - 1];
      ++i;
    }
  return (dst.n);
}

t_ullint		ls_swapll(t_ullint val)
{
  t_ullint		i;
  union u_tabll		src;
  union u_tabll		dst;

  i = 0;
  src.n = val;
  dst.n = 0;
  while (i < sizeof(t_ullint))
    {
      dst.c[i] = src.c[sizeof(t_ullint) - i - 1];
      ++i;
    }
  return (dst.n);
}
