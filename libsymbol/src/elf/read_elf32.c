#include <sys/types.h>
#include <elf.h>
#include <string.h>
#include "libsymbol.h"
#include "libsymbol-elf.h"

static void	rel_info(t_memfile *mem, t_elffile32 *elf,
			 int i)
{
  Elf32_Shdr	*reldyn;

  reldyn = (Elf32_Shdr *)(elf->sect + elf->sect[i].sh_link);
  elf->dynsym = (Elf32_Sym *)((size_t)mem->ptr + reldyn->sh_offset);
  elf->dynstr =
    (char *)((size_t)mem->ptr + elf->sect[reldyn->sh_link].sh_offset);
}

static void	rela_info(t_memfile *mem, t_elffile32 *elf,
			 int i)
{
  Elf32_Shdr	*reladyn;

  reladyn = (Elf32_Shdr *)(elf->sect + elf->sect[i].sh_link);
  elf->dynsym = (Elf32_Sym *)((size_t)mem->ptr + reladyn->sh_offset);
  elf->dynstr =
    (char *)((size_t)mem->ptr + elf->sect[reladyn->sh_link].sh_offset);
}

static void	inspect_section32(t_memfile *mem, t_elffile32 *elf,
				  int i, int *st)
{
  if (elf->sect[i].sh_type == SHT_SYMTAB)
    {
      elf->sym_count = elf->sect[i].sh_size / elf->sect[i].sh_entsize;
      elf->symnames =
	(char *)(mem->ptr + elf->sect[elf->sect[i].sh_link].sh_offset);
      elf->symbol = (Elf32_Sym *)(mem->ptr + elf->sect[i].sh_offset);
      *st = 0;
    }
  else if (elf->sect[i].sh_type == SHT_REL)
    {
      elf->rel_count = elf->sect[i].sh_size / elf->sect[i].sh_entsize;
      elf->rel = (Elf32_Rel *)(mem->ptr + elf->sect[i].sh_offset);
      rel_info(mem, elf, i);
    }
  else if (elf->sect[i].sh_type == SHT_RELA)
    {
      elf->rela_count = elf->sect[i].sh_size / elf->sect[i].sh_entsize;
      elf->rela = (Elf32_Rela *)(mem->ptr + elf->sect[i].sh_offset);
      rela_info(mem, elf, i);
    }
  else if (elf->sect[i].sh_type == SHT_PROGBITS
	   && !strncmp(elf->sectnames + elf->sect[i].sh_name, ".plt", 4))
    elf->plt = (char *)((size_t)elf->sect[i].sh_addr);
}

int		read_elf32(t_memfile *mem, t_elffile32 *elf)
{
  int		i;
  int		st;

  elf->ehdr = mem->ptr;
  elf->sect = (Elf32_Shdr *)(mem->ptr + elf->ehdr->e_shoff);
  elf->sect_count = elf->ehdr->e_shnum;
  elf->sectnames =
    (char *)(mem->ptr + elf->sect[elf->ehdr->e_shstrndx].sh_offset);
  i = 0;
  st = -1;
  while (i < elf->ehdr->e_shnum)
    {
      inspect_section32(mem, elf, i, &st);
      ++i;
    }
  return (st);
}
