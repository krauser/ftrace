#include <sys/types.h>
#include <elf.h>
#include <string.h>
#include "libsymbol.h"
#include "libsymbol-elf.h"

int		read_elf(t_memfile *mem,
			 t_elffile *elf)
{
  unsigned char	*e_ident;

  e_ident = (unsigned char*)(mem->ptr);
  if (memcmp(e_ident, ELFMAG, SELFMAG) != 0)
    return (-1);
  if (e_ident[EI_CLASS] == ELFCLASS32)
    elf->arch = LS_ARCH_X86;
  else if (e_ident[EI_CLASS] == ELFCLASS64)
    elf->arch = LS_ARCH_X64;
  else
    return (-1);
  return (0);
}
