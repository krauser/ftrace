#include <elf.h>
#include <stdlib.h>
#include <string.h>
#include "libsymbol.h"
#include "libsymbol-elf.h"

t_symbol	*get_sym_elf32(t_memfile *);
t_symbol	*get_sym_elf64(t_memfile *);

t_symbol	*get_symbols(const char *bin)
{
  t_memfile	mem;
  t_elffile	elf;

  if (map_file(bin, &mem) == -1)
    return (NULL);

  if (read_elf(&mem, &elf) == -1)
    {
      (void)unmap_file(&mem);
      return (NULL);
    }
  if (elf.arch == LS_ARCH_X86)
    return (get_sym_elf32(&mem));
  else if (elf.arch == LS_ARCH_X64)
    return (get_sym_elf64(&mem));
  (void)unmap_file(&mem);
  return (NULL);
}
