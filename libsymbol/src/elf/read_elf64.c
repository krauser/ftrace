#include <sys/types.h>
#include <elf.h>
#include <string.h>
#include "libsymbol.h"
#include "libsymbol-elf.h"

static void	rel_info(t_memfile *mem, t_elffile64 *elf,
			 int i)
{
  Elf64_Shdr	*reldyn;

  reldyn = (Elf64_Shdr *)(elf->sect + elf->sect[i].sh_link);
  elf->dynsym = (Elf64_Sym *)((size_t)mem->ptr + (size_t)reldyn->sh_offset);
  elf->dynstr =
    (char *)((size_t)mem->ptr + (size_t)elf->sect[reldyn->sh_link].sh_offset);
}

static void	rela_info(t_memfile *mem, t_elffile64 *elf,
			 int i)
{
  Elf64_Shdr	*reladyn;

  reladyn = (Elf64_Shdr *)(elf->sect + elf->sect[i].sh_link);
  elf->dynsym = (Elf64_Sym *)((size_t)mem->ptr + (size_t)reladyn->sh_offset);
  elf->dynstr =
    (char *)((size_t)mem->ptr + (size_t)elf->sect[reladyn->sh_link].sh_offset);
}

static void	inspect_section64(t_memfile *mem, t_elffile64 *elf,
				  int i, int *st)
{
  if (elf->sect[i].sh_type == SHT_SYMTAB)
    {
      elf->sym_count = elf->sect[i].sh_size / elf->sect[i].sh_entsize;
      elf->symnames =
	(char *)(mem->ptr + elf->sect[elf->sect[i].sh_link].sh_offset);
      elf->symbol = (Elf64_Sym *)(mem->ptr + elf->sect[i].sh_offset);
      *st = 0;
    }
  else if (elf->sect[i].sh_type == SHT_REL)
    {
      elf->rel_count = elf->sect[i].sh_size / elf->sect[i].sh_entsize;
      elf->rel = (Elf64_Rel *)(mem->ptr + elf->sect[i].sh_offset);
      rel_info(mem, elf, i);
    }
  else if (elf->sect[i].sh_type == SHT_RELA)
    {
      elf->rela_count = elf->sect[i].sh_size / elf->sect[i].sh_entsize;
      elf->rela = (Elf64_Rela *)(mem->ptr + elf->sect[i].sh_offset);
      rela_info(mem, elf, i);
    }
  else if (elf->sect[i].sh_type == SHT_PROGBITS
	   && !strcmp(elf->sectnames + elf->sect[i].sh_name, ".plt"))
    elf->plt = (char *)(elf->sect[i].sh_addr);
}

int		read_elf64(t_memfile *mem, t_elffile64 *elf)
{
  int		i;
  int		st;

  elf->ehdr = mem->ptr;
  elf->sect = (Elf64_Shdr *)(mem->ptr + elf->ehdr->e_shoff);
  elf->sect_count = elf->ehdr->e_shnum;
  elf->sectnames =
    (char *)(mem->ptr + elf->sect[elf->ehdr->e_shstrndx].sh_offset);
  i = 0;
  st = -1;
  while (i < elf->ehdr->e_shnum)
    {
      inspect_section64(mem, elf, i, &st);
      ++i;
    }
  return (st);
}
