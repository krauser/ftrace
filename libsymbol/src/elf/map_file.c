#include <fcntl.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include "libsymbol.h"
#include "libsymbol-macho.h"

int		map_file(const char *file,
			 t_memfile *stru)
{
  struct stat	mstat;

  stru->fd = open(file, O_RDONLY);
  if (stru->fd == -1)
    return (-1);
  if (fstat(stru->fd, &mstat) == -1)
    {
      close(stru->fd);
      return (-2);
    }
  stru->size = mstat.st_size;
  stru->ptr = mmap(NULL, mstat.st_size, PROT_READ, MAP_PRIVATE,
		   stru->fd, 0);
  if (stru->ptr == MAP_FAILED)
    return (-3);
  return (0);
}
