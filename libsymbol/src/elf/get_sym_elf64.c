#include <elf.h>
#include <stdlib.h>
#include <string.h>
#include "libsymbol.h"
#include "libsymbol-elf.h"

static unsigned int	get_rel_sym(t_symbol *ret, t_elffile64 *elf,
				    unsigned int i)
{
  unsigned int		u;
  unsigned int		type;
  unsigned int		sym;

  u = 0;
  while (u < elf->rel_count)
    {
      type = ELF64_R_TYPE(elf->rel[u].r_info);
      sym = ELF64_R_SYM(elf->rel[u].r_info);
      if (type == R_X86_64_JUMP_SLOT)
	{
	  if (!(ret[i].name = strdup(elf->dynstr + elf->dynsym[sym].st_name)))
	    u = elf->rel_count;
	  else
	    ret[i++].address = (void *)(size_t)(elf->rel[u].r_offset);
	}
      ++u;
    }
  return (i);
}

static unsigned int	get_rela_sym(t_symbol *ret, t_elffile64 *elf,
				    unsigned int i)
{
  unsigned int		u;
  unsigned int		type;
  unsigned int		sym;

  u = 0;
  while (u < elf->rela_count)
    {
      type = ELF64_R_TYPE(elf->rela[u].r_info);
      sym = ELF64_R_SYM(elf->rela[u].r_info);
      if (type == R_X86_64_JUMP_SLOT)
	{
	  if (!(ret[i].name = strdup(elf->dynstr + elf->dynsym[sym].st_name)))
	    u = elf->rela_count;
	  else
	    ret[i++].address = (void *)(size_t)(elf->plt + 16 * sym);
	}
      ++u;
    }
  return (i);
}

static void	get_dyn_sym(t_symbol *ret, t_elffile64 *elf, unsigned int i)
{
  unsigned int	j;

  j = get_rela_sym(ret, elf, get_rel_sym(ret, elf, i));
  ret[j].name = (char *)-1;
  ret[j].address = (void *)-1;
}

static t_symbol	*get_sym_elf64_loop(t_symbol *ret, t_elffile64 *elf,
				    t_memfile *mem)
{
  unsigned int	i;
  unsigned int	j;

  i = 1;
  j = 0;
  while (i < elf->sym_count)
    {
      if ((elf->symbol[i].st_info & 0x0f) == STT_FUNC)
      {
	if (!(ret[j].name = strdup(elf->symnames + elf->symbol[i].st_name)))
	  i = elf->sym_count;
	else
	  ret[j++].address = (void *)((long)(elf->symbol[i].st_value));
      }
      ++i;
    }
  get_dyn_sym(ret, elf, j);
  (void)unmap_file(mem);
  return (ret);
}

t_symbol	*get_sym_elf64(t_memfile *mem)
{
  t_elffile64	elf;
  t_symbol	*ret;

  memset(&elf, 0, sizeof(elf));
  if (read_elf64(mem, &elf) == -1)
    {
      (void)unmap_file(mem);
      return (NULL);
    }
  if (!(ret = malloc(sizeof(*ret) *
		     (elf.sym_count + elf.rel_count + elf.rela_count + 1))))
    {
      (void)unmap_file(mem);
      return (NULL);
    }
  return (get_sym_elf64_loop(ret, &elf, mem));
}
