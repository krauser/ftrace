#include <stdio.h>
#include <stdlib.h>
#include "libsymbol.h"

/*
** Display symbols on the binary
*/
static void	_print_symbol(t_symbol *sym)
{
  unsigned int	i;

  if (sym)
    {
      i = -1;
      while (sym[++i].address != (void *)-1)
	{
	  if (sym[i].address)
	    printf("0x%016lx: %s\n", (unsigned long)sym[i].address, sym[i].name);
	  else
	    printf("%18s: %s\n", " ", sym[i].name);
	}
      free(sym);
    }
}

int		main(int argc, char **argv)
{
  t_symbol	*sym;

  sym = NULL;
  if (argc >= 2)
    {
      sym = get_symbols(argv[1]);
      if (sym)
	_print_symbol(sym);
      else
	fprintf(stderr, "Error: no symbol in [%s]\n", argv[1]);
    }
  else
    fprintf(stderr, "Usage: ./mini_nm ./BIN\n");
  return (0);
}
