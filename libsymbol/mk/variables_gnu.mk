ISELF		 = ${shell test -f /usr/include/mach-o/loader.h ; echo $$?}

ifeq "$(ISELF)" "1"
CFLAGS		+= -DLIBSYMBOL_FORMAT_ELF
SRCDIR		 = src/elf
SOURCES		 = $(SOURCES_ELF)
else
SOURCES		 = $(SOURCES_MACH_O)
CFLAGS		+= -DLIBSYMBOL_FORMAT_MACH_O
SRCDIR		 = src/mach-o
endif

SYSTEM		 = ${shell uname | tr '[:lower:]' '[:upper:]' | tr '-' '_'}
ARCH		 = ${shell uname -m | tr '[:lower:]' '[:upper:]' | tr '-' '_'}

ifeq "$(SYSTEM)" "DARWIN"
CFLAGS		+= -arch i386 -arch x86_64 -arch ppc
endif

SRC		 = ${addprefix $(SRCDIR)/,$(SOURCES)}
OBJ		 = ${addprefix $(OBJDIR)/,$(SOURCES:.c=.o)}

CFLAGS		+= -DLIBSYMBOL_SYSTEM_$(SYSTEM) -I$(HDRDIR)
