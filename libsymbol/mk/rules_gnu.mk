include mk/variables.mk
include mk/variables_gnu.mk

$(NAME):	$(OBJDIR) $(OBJ)
	@echo $(CFLAGS)
	$(AR) $(NAME) $(OBJ)
	$(CC) $(CFLAGS) -o $(OBJDIR)/mini_nm.o $(CFLAGS) -c src/mini_nm.c
	$(CC) -o mini-nm $(OBJ) $(OBJDIR)/mini_nm.o

all:		$(NAME)

$(OBJDIR)/%.o:	$(SRCDIR)/%.c
	$(CC) -o $@ $(CFLAGS) -c $<

$(OBJDIR):
	$(MKDIR) $(OBJDIR)

clean:
	$(RM) $(OBJ)

fclean:		clean
	$(RM) $(NAME)

re:		fclean $(NAME)
