.include "variables.mk"
.include "variables_bsd.mk"

$(NAME):	$(OBJDIR) $(OBJ)
	$(AR) $(NAME) $(OBJ)
	$(CC) -arch i386 -arch x86_64 -arch ppc -o $(OBJDIR)/mini_nm.o $(CFLAGS) -c src/mini_nm.c
	$(CC) -arch i386 -arch x86_64 -arch ppc -o mini-nm $(OBJ) $(OBJDIR)/mini_nm.o

.SUFFIXES:		.o .c

.for i in $(SOURCES)
$(OBJDIR)/${i:.c=.o}:	$(SRCDIR)/${i}
	$(CC) -o ${.TARGET} $(CFLAGS) -c $(SRCDIR)/${i}
.endfor

$(OBJDIR):
	$(MKDIR) $(OBJDIR)

clean:
	$(RM) $(OBJ)

fclean:		clean
	$(RM) $(NAME)

re:		clean $(NAME)
