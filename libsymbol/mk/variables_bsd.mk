.if exists(/usr/include/mach-o/loader.h)
CFLAGS		+= -DLIBSYMBOL_FORMAT_MACH_O
SRCDIR		 = src/mach-o
SOURCES		 = $(SOURCES_MACH_O)
.else
CFLAGS		+= -DLIBSYMBOL_FORMAT_ELF
SRCDIR		 = src/elf
SOURCES		 = $(SOURCES_ELF)
.endif

SYSTEM		!= uname | tr '[:lower:]' '[:upper:]' | tr '-' '_'
ARCH		!= uname -m | tr '[:lower:]' '[:upper:]' | tr '-' '_'

.if $(SYSTEM) == "DARWIN"
CFLAGS		+= -arch i386 -arch x86_64 -arch ppc
.endif

SRC		 = $(SOURCES:S/^/$(SRCDIR)\//)
OBJ		 = $(SOURCES:S/^/$(OBJDIR)\//:S/.c/.o/)

CFLAGS		+= -DLIBSYMBOL_SYSTEM_$(SYSTEM) -I$(HDRDIR)
