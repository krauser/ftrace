NAME		 = libsymbol.a

CC		 = gcc
CFLAGS		+= -Wall -Wextra -Werror -Wfatal-errors -pipe
AR		 = ar rcs
RM		 = rm -f
MKDIR		 = mkdir -p
RUBY		 = ruby

# SRCDIR is set by _gnu / _bsd files
# SRCDIR	 = src
OBJDIR		 = build
HDRDIR		 = hdr

SOURCES_ELF	 = map_file.c \
		   unmap_file.c \
		   read_elf.c \
		   read_elf32.c \
		   read_elf64.c \
		   get_symbols.c \
		   get_sym_elf32.c \
		   get_sym_elf64.c
SOURCES_MACH_O	 = map_file.c \
		   unmap_file.c \
		   file_type.c \
		   read_bin.c \
		   read_fat.c \
		   ls_swap.c \
		   which_bin.c \
		   ls_bintocur.c \
		   next_command.c \
		   read_mach.c \
		   get_symbols.c \
		   platform.c \
		   get_bin_checked.c
