#!/bin/sh

if [ "$(uname)" == "FreeBSD" ]
then
    echo "make -C"
else
    echo "./bsd-make-wrap.sh"
fi
