/*
** print_graph_statistique.c for  in /home/lauren_k/teck2/ftrace/ftrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Fri Jun 29 20:50:00 2012 jean-baptiste laurent
** Last update Sat Jun 30 23:21:56 2012 jean-baptiste laurent
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "ftrace.h"

static void	_print_stat(t_st const *st, size_t src, t_symbol const *sym)
{
  if (st->call || st->returned || st->called || st->ret || st->syscall)
  {
    printf("\"%#lx\" [shape=component, label=\"==> Fonction : ", (long)src);
    find_fct_name(src, sym);
  }
  if (st->call && st->returned)
    printf("  %u call ->|<- returned %u\\n", st->call, st->returned);
  else
    if (st->call)
      printf("call = %u\\n", st->call);
    else if (st->returned)
      printf("returned = %u\\n", st->returned);
  if (st->called && st->ret)
    printf("%u called ->|<- return %u\\n", st->called, st->ret);
  else
  {
    if (st->called)
      printf("called = %u\\n", st->called);
    if (st->ret)
      printf("ret = %u\\n", st->ret);
  }
  if (st->syscall)
    printf("syscall = %u\\n", st->syscall);
  if (st->call || st->returned || st->called || st->ret || st->syscall)
    printf("\"];\n");
}

static __inline__ int	_isInFct(size_t curr, size_t fct, size_t fct_after)
{
  return ((curr >= fct && curr < fct_after));
}

static void		_declare_node_on_block(t_list const *call_ref_list,
					       t_list const *fct_list,
					       size_t fct[2],
					       t_symbol const *sym)
{
  t_branch const	*br;
  t_st			st;

  memset(&st, 0, sizeof(st));
  while (call_ref_list)
  {
    br = call_ref_list->data;
    st.fct_dest = get_curr_addr(fct_list, br->dest_addr);
    st.fct_src = get_curr_addr(fct_list, br->op_addr);
    if (br && IS_CALL(br->opcode) && _isInFct(st.fct_dest, fct[0], fct[1]))
      st.called += br->occurence;
    if (br && IS_CALL(br->opcode) &&_isInFct(st.fct_src, fct[0], fct[1]))
      st.call += br->occurence;
    else if (br && IS_RET(br->opcode) && _isInFct(st.fct_dest, fct[0], fct[1]))
      st.returned += br->occurence;
    else if (br && IS_RET(br->opcode) && _isInFct(st.fct_src, fct[0], fct[1]))
      st.ret += br->occurence;
    else if (br && is_sys(br->opcode) &&_isInFct(st.fct_src, fct[0], fct[1]))
      st.syscall += br->occurence;
    call_ref_list = call_ref_list->next;
  }
  _print_stat(&st, fct[0], sym);
}

static void		_declare_block(t_list const *call_ref_list,
				       t_list const *fct_list,
				       t_symbol const *sym)
{
  size_t		fct[2];
  t_list const		*tmp;

  if (fct_list)
  {
    tmp = fct_list;
    fct[0] = *((size_t const *)tmp->data);
    tmp = tmp->next;
    while (tmp)
    {
      fct[1] = *((size_t const *)tmp->data);
      _declare_node_on_block(call_ref_list, fct_list, fct, sym);
      fct[0] = fct[1];
      tmp = tmp->next;
    }
  }
}

/*
** Print statistique for each fonction
*/
void		print_statistique(t_list const *fct_list,
				  t_list const *call_ref_list,
				  t_symbol const *sym)
{
  (void)fct_list;
  (void)call_ref_list;
  _declare_block(call_ref_list, fct_list, sym);
}
