/*
** aff_syscall.c for  in /home/lauren_k/teck2/strace
**
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
**
** Started on  Thu May  3 17:23:01 2012 jean-baptiste laurent
** Last update Sun Jul  1 19:07:42 2012 jean-baptiste laurent
*/

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "ftrace.h"
#include "errno_msg.h"

/*
** Print the return value of the syscall
** Print errno in case of error
*/
static void	aff_sys_return_value(t_regtab const *reg_after)
{
  if (reg_after)
  {
    fprintf(stderr, "%#lx\n", reg_after->rax);
  }
}

/*
** Print the name of the syscall and return his prototype in 32 or 64 bits
** TODO: 32/64 tableau de nom des syscalls
*/
static void	aff_sys_name(t_regtab const *reg_before, int _32_64)
{
  size_t	rax;

  rax = reg_before->rax;
  if (_32_64 == 32)
    fprintf(stderr, "%s", lt_syscallname(rax));
  else if (_32_64 == 64)
    fprintf(stderr, "%s", lt_syscallname(rax));
  else
    fprintf(stderr, "syscall_%lu", (unsigned long)rax);
}

/*
** Print syscall info
*/
void		aff_syscall(t_ftrace *info, int pid,
			    t_regtab const *reg_before,
			    t_regtab const *reg_after)
{
  t_branch	br;

  (void)pid;
  memset(&br, 0, sizeof(br));
  if (reg_before && info)
  {
    if (S_RIP_PTR(info->opt.opt))
      fprintf(stderr, "[%16lx] ", reg_before->rip);
    aff_sys_name(reg_before, 64);
    fprintf(stderr, "()		->");
    aff_sys_return_value(reg_after);

    if (reg_after)
    {
      br.name = strdup(lt_syscallname(reg_before->rax));
      br.occurence = 1;
      br.opcode = 0x80cd;
      br.op_addr = reg_before->rip;
      br.dest_addr = reg_after->rip;
      add_elem_on_graph(&info->call_ret_list, &br);
    }
  }
  else
    fprintf(stderr, "aff_syscall: reg_before is NULL\n");
}
