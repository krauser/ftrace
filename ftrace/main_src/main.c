/*
** main.c for  in /home/lauren_k/teck2/strace
**
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
**
** Started on  Thu May  3 16:14:15 2012 jean-baptiste laurent
** Last update Tue Jul  3 15:48:56 2012 jean-baptiste brenaut
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "ftrace.h"

static size_t	_get_offset(char const *fct_name, t_symbol const *sym)
{
  unsigned int	i;

  i = 0;
  if (sym && fct_name)
  {
    while (sym[i].address != (void *)-1 && strcmp(fct_name, sym[i].name))
      ++i;
    if (sym[i].address != (void *)-1)
      return ((size_t)sym[i].address);
  }
  return (0);
}

static t_list	*_epur_after_ret(t_list *call_ret_list, size_t addr,
				 t_symbol const *sym)
{
  t_list	*last;
  t_branch	*br;

  if (call_ret_list && addr)
  {
    last = list_last_elem(call_ret_list);
    while (last && (br = last->data)
	   && !(IS_RET(br->opcode)
		&& get_curr_addr_sym(sym, (void *)br->op_addr) == addr))
    {
      if (is_sys(br->opcode))
	free(br->name);
      list_pop_back(&call_ret_list);
      last = list_last_elem(call_ret_list);
    }
  }
  return (call_ret_list);
}

static void	_epur_list(t_list **call_ret_list,
			   char const *fct_name, t_symbol const *sym,
			   size_t *prog_start)
{
  size_t	addr;
  t_branch	*br;

  addr = _get_offset(fct_name, sym);
  if (addr && call_ret_list && *call_ret_list && fct_name && sym)
  {
    while (*call_ret_list && (br = (*call_ret_list)->data)
	   && br->dest_addr != addr)
    {
      if (is_sys(br->opcode))
	free(br->name);
      list_pop_front(call_ret_list);
    }
    if (*call_ret_list && br)
      *prog_start = br->dest_addr;
    list_pop_front(call_ret_list);
    if (*call_ret_list)
      *call_ret_list = _epur_after_ret(*call_ret_list, addr, sym);
  }
  else if (!addr && fct_name)
    fprintf(stderr, "Fonction [%s] not found\n", fct_name);
}

/*
** Free syscall
*/
static void	_free_syscall_strdup(t_list *list, t_symbol *sym)
{
  t_branch	*br;

  while (list && (br = list->data))
  {
    if (is_sys(br->opcode))
    {
      free(br->name);
      br->name = NULL;
    }
    list = list->next;
  }
  free(sym);
}

/*
** Entry point of the program
*/
int		main(int argc, char **argv)
{
  int		ret;
  t_ftrace	info;
  t_symbol	*sym;

  memset(&info, 0, sizeof(info));
  ret = ftrace_opt_parser(&info.opt, argc, argv);
  if (ret < argc && argc > 1 && !S_USAGE(info.opt.opt)
      && signal(SIGINT, catch_sig) != SIG_ERR
      && signal(SIGTERM, catch_sig) != SIG_ERR)
  {
    ftrace_main_loop(argv[ret], &argv[ret], &info);
    sym = get_symbols((info.opt.prog_name ? info.opt.prog_name : ""));
    _epur_list(&info.call_ret_list, info.opt.fct_name, sym, &info.prog_start);
    make_dotty_graph(info.call_ret_list, &info, sym);
    _free_syscall_strdup(info.call_ret_list, sym);
    free_list(&info.call_ret_list);
    sym = NULL;
    info.call_ret_list = NULL;
    fprintf(stderr, "End of tracing ...\n");
  }
  else if (S_USAGE(info.opt.opt) || (ret == 0 && errno == 0))
    fprintf(stderr, "%s\n", USAGE);
  else
    perror("==> Ftrace");
  return (EXIT_SUCCESS);
}
