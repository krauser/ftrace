/*
** print_dotty_graph.c for  in /home/lauren_k/teck2/ftrace/ftrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Thu Jun 28 12:24:16 2012 jean-baptiste laurent
** Last update Sun Jul  1 19:15:24 2012 jean-baptiste laurent
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "ftrace.h"

void		find_fct_name(size_t addr, t_symbol const *sym)
{
  unsigned int	i;

  i = 0;
  if (sym)
  {
    while (sym[i].address != (void *)-1
	   && sym[i].address != (void const *)addr)
      ++i;
    if (sym[i].address != (void *)-1)
    {
      printf("%s <==\\n============\\n", sym[i].name);
      return ;
    }
  }
  printf("%#lx\\n\\n", (long)addr);
}

int			compare_addr(void const *a, void const *b)
{
  void	const		*_a;
  void	const		*_b;

  if (a && b)
  {
    _a = *(void const **)a;
    _b = *(void const **)b;
    if (_a == _b)
      return (0);
    else if (_a > _b)
      return (1);
  }
  return (-1);
}

size_t		get_curr_addr(t_list const *fct_list, size_t addr)
{
  size_t	src;

  src = 0x00;
  while (fct_list && fct_list->next)
  {
    if (compare_addr(&addr, fct_list->data) >= 0
	&& compare_addr(&addr, fct_list->next->data) < 0)
      src = *((size_t *)fct_list->data);
    fct_list = fct_list->next;
  }
  return (src);
}

size_t		get_curr_addr_sym(t_symbol const *sym, void *addr)
{
  unsigned int	i;
  size_t	after;
  size_t	before;
  size_t	c;

  if (sym && addr)
  {
    after = (size_t)sym[0].address;
    before = (size_t)sym[0].address;
    i = -1;
    while ((c = (size_t)sym[++i].address) != (size_t)-1)
    {
      if ((size_t)addr < c && ((size_t)addr - c) > ((size_t)addr - after))
	after = c;
      if ((size_t)addr >= c && ((size_t)addr - c) < ((size_t)addr - before))
	before = c;
    }
    if (before <= (size_t)addr && (size_t)addr < after)
      return (before);
  }
  return (0);
}

/*
** Display link between node (call / ret / syscall)
*/
void		print_link(t_list const *fct_list, t_branch const *curr)
{
  size_t	src;
  size_t	dest;

  src = get_curr_addr(fct_list, curr->op_addr);
  dest = get_curr_addr(fct_list, curr->dest_addr);
  if (IS_CALL(curr->opcode))
    printf("\"%#lx\" -> \"%#lx\""
    	   "[label=\"%s: %u\", color=%s, arrowhead=open];\n",
    	   (long)src, (long)dest, "call", curr->occurence, "green");
  else if (IS_RET(curr->opcode))
    printf("\"%#lx\" -> \"%#lx\""
	   "[label=\"%s: %u\", color=%s, arrowhead=open];\n",
    	   (long)src, (long)dest, "ret", curr->occurence, "blue");
  else if (is_sys(curr->opcode))
  {
    printf("\"%#lx:%s\" [shape=box, color=red, penwidth=3, label=\"%s\"];",
	   (long)src, curr->name, curr->name);
    printf("\"%#lx\" -> \"%#lx:%s\" [label=\"%s: %u\", color=%s, "
	   "arrowhead=open];\n",
   	   (long)src, (long)src, curr->name, "syscall",
	   curr->occurence, "black");
  }
}
