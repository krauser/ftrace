/*
** ftrace_core.c for  in /home/lauren_k/teck2/ftrace/ftrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sat Jun 30 20:56:47 2012 jean-baptiste laurent
** Last update Sat Jun 30 23:26:39 2012 jean-baptiste laurent
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "ftrace.h"

static int	g_strace_end = 0;
static void	(*fct[3])(t_ftrace *, pid_t pid,
			  t_regtab const *, t_regtab const *) =
{
  &aff_call,
  &aff_ret,
  &aff_syscall
};

/*
**
*/
void		catch_sig(int sig)
{
  fprintf(stderr, "Signal %d received. Exiting ...\n", sig);
  detach_proc();
  exit(EXIT_FAILURE);
  g_strace_end = 1;
}

/*
** Create a new process and trace it
*/
static pid_t	create_proc(char *name, char **argv, t_opt *opt)
{
  int		status;
  pid_t		pid;

  if ((pid = attach_proc(opt)) == 0)
  {
    pid = fork();
    if (pid == 0)
    {
      if (lt_traceme() == 0)
      {
	(void)close(1);
	(void)close(2);
	execvp(name, argv);
	perror("execlp");
      }
    }
    else if (pid > 0 && waitpid(pid, &status, 0) == pid
	     && WIFSTOPPED(status) && WSTOPSIG(status) == SIGTRAP)
      return (pid);
    else
      perror("fork");
  }
  else
    return (pid);
  return (-1);
}

static int	get_fct(int ret)
{
  if (is_sys(ret))
    return (2);
  else if (is_ret(ret))
    return (1);
  else
    return (0);
}

/*
** The main loop of the program
*/
void		ftrace_main_loop(char *name, char **argv, t_ftrace *info)
{
  int		ret;
  int		op;
  pid_t		pid;
  t_regtab	reg_before;
  t_regtab	reg_after;

  ret = 0;
  pid = create_proc(name, argv, &info->opt);
  errno = 0;
  if (pid != -1)
    while (!errno && !ret && !g_strace_end)
    {
      lt_getregs(pid, &reg_before);
      if (!info->prog_start)
	info->prog_start = reg_before.rip;
      ret = lt_getdata(pid, (void *)(reg_before.rip));
      op = ret;
      if (!(errno = 0) && (is_sys(ret) || is_call(ret) || is_ret(ret)))
	if ((ret = go_to_next_inst(pid)) == 0 && !lt_getregs(pid, &reg_after))
	  fct[get_fct(op)](info, pid, &reg_before, &reg_after);
	else
	  fct[get_fct(op)](info, pid, &reg_before, NULL);
      else
	ret = go_to_next_inst(pid);
    }
}
