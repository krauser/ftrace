/*
** strace_core.c for  in /home/lauren_k/teck2/ftrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Mon Jun 25 12:45:33 2012 jean-baptiste laurent
** Last update Mon Jun 25 14:16:57 2012 jean-baptiste laurent
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "ftrace.h"

/*
** Test if the current instruction pointe on a syscall
** Return true or false
*/
int		is_sys(int inst)
{
  union
  {
    int		a;
    char	tab[4];
  }		endian;

  endian.a = 1;
  if (endian.tab[0])
    return (IS_SYS_L(inst));
  else if (endian.tab[1])
    return (IS_SYS_B(inst));
  else
  {
    fprintf(stderr, "Error: strace implemented only for little/big endian\n");
    exit(EXIT_FAILURE);
  }
  return (false);
}
