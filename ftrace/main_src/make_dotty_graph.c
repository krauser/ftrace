/*
** make_dotty_graph.c for  in /home/lauren_k/teck2/ftrace/ftrace
**
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
**
** Started on  Tue Jun 26 15:17:14 2012 jean-baptiste laurent
** Last update Sun Jul  1 19:09:08 2012 jean-baptiste laurent
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "ftrace.h"

static void		make_fct_list_fct(t_symbol const *sym,
					  t_list *fct_list)
{
  unsigned int	i;

  i = 0;
  if (sym)
  {
    while (sym[i].address != (void *)-1)
    {
      if (list_get_data(fct_list, &sym[i].address, &compare_addr) == NULL)
	list_push_sort(&fct_list, &sym[i].address, sizeof(void *),
		       &compare_addr);
      ++i;
    }
  }
}

/*
** Collect all destination of call and order them
*/
static t_list		*make_fct_list(t_list const *call_ref_list,
				       size_t prog_start)
{
  void			*addr;
  t_list		*fct;
  t_branch const	*br;

  fct = NULL;
  addr = (void *)-1;
  if (list_push_front(&fct, &addr, sizeof(void *)) == NULL)
    abort();
  if (list_push_front(&fct, (void *)&prog_start, sizeof(void *)) == NULL)
    abort();
  addr = (void *)0;
  if (list_push_front(&fct, &addr, sizeof(void *)) == NULL)
    abort();
  while (call_ref_list)
  {
    br = call_ref_list->data;
    if (br && IS_CALL(br->opcode)
	&& list_get_data(fct, &br->dest_addr, &compare_addr) == NULL)
      if (list_push_sort(&fct, (void *)&br->dest_addr,
      			 sizeof(void *), &compare_addr) == NULL)
      	abort();
    call_ref_list = call_ref_list->next;
  }
  return (fct);
}

void			make_dotty_graph(t_list const *call_ref_list,
					 t_ftrace const *info,
					 t_symbol const *sym)
{
  t_branch const	*br;
  t_list		*fct_list;

  fprintf(stderr, "%s\n", info->opt.file);
  printf("digraph ftrace {\n""rankdir=LR;\n""concentrate=true;\n"
	 "compound=true;\n""fontsize=14;\n""fontname=\"Times-Roman\";\n"
	 "fontcolor=black;\n""root=\"%#lx\";\n""compound=true;\n",
	 (long)info->prog_start);
  printf("\"%#lx\" [shape=box3d, root=true, penwidth=5];",
	 (long)info->prog_start);
  fct_list = make_fct_list(call_ref_list, info->prog_start);
  make_fct_list_fct(sym, fct_list);
  if (S_VERBOSE(info->opt.opt))
    print_statistique(fct_list, call_ref_list, sym);
  while (fct_list && call_ref_list)
  {
    br = (t_branch const *)call_ref_list->data;
    print_link(fct_list, br);
    if (call_ref_list)
      call_ref_list = call_ref_list->next;
  }
  free_list(&fct_list);
  printf("}\n");
}
