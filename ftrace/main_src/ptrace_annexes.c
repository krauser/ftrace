/*
** ptrace_annexes.c for  in /home/lauren_k/teck2/strace
**
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
**
** Started on  Sun May  6 21:56:45 2012 jean-baptiste laurent
** Last update Sun Jul  1 19:06:18 2012 jean-baptiste laurent
*/

#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "ftrace.h"
#include "signal_tab.h"
/*
** Print a message for the death of the program
*/
static int	_manage_end_of_process(int const status, int pid)
{
  size_t	sig;

  (void)pid;
  if (WIFEXITED(status))
    fprintf(stderr, S_EXIT_MSG, WEXITSTATUS(status));
  else if (WIFSIGNALED(status) || WIFSTOPPED(status))
  {
    sig = (WIFSIGNALED(status) ? WTERMSIG(status) : WSTOPSIG(status));
    if (sig < ((sizeof(signal_tab)) / (sizeof(*signal_tab)))
	&& sig != SIGCHLD && sig != SIGSTOP)
      fprintf(stderr, S_KILLED_SIG_MSG, signal_tab[sig - 1].sig_def);
    else if (sig != SIGCHLD && sig != SIGSTOP)
      fprintf(stderr, S_KILLED_USIG_MSG, (long)sig);
    else
      return (0);
  }
  else
    fprintf(stderr, S_UNKNOWN_MSG);
  return (-1);
}

/*
** Step one instruction of the child
*/
int	go_to_next_inst(int pid)
{
  long	ret;
  int	status;

  ret = lt_singlestep(pid);
  if (ret == 0 && waitpid(pid, &status, 0) == pid
      && WIFSTOPPED(status) && WSTOPSIG(status) == SIGTRAP)
  {
    return (0);
  }
  else
    return (_manage_end_of_process(status, pid));
}

/*
** Get size byte of data from the addr 'child_addr' to '*ptr'
** Return the number of byte read
*/
size_t		fill_data_from_process(int pid, long child_addr,
				       void *ptr, size_t size)
{
  size_t	ret;
  long		pt_ret;

  ret = 0;
  if (ptr)
  {
    errno = 0;
    while (errno == 0 && ret <= size)
    {
      pt_ret = lt_getdata(pid, (void *)child_addr);
      if (errno == 0)
      {
	memcpy((void *)((size_t)ptr + ret), &pt_ret, sizeof(int));
	ret = ret + sizeof(int);
	child_addr = child_addr + sizeof(int);
      }
    }
    if (errno == 0 && ret > size)
      ret = size;
  }
  return (ret);
}
