/*
** modrm_annexes.c for  in /home/lauren_k/teck2/ftrace/ftrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun Jul  1 14:09:38 2012 jean-baptiste laurent
** Last update Sun Jul  1 14:09:49 2012 jean-baptiste laurent
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>
#include "ftrace.h"

void		fill_modrm_struct(int inst, t_modrm *modrm)
{
  modrm->mod = MODRM_GET_MOD(inst >> 8);
  modrm->reg = MODRM_GET_REG(inst >> 8);
  modrm->rm = MODRM_GET_RM(inst >> 8);
}

void		fill_sib_struct(int inst, t_sib *sib)
{
  sib->ss = SIB_GET_SCALE(inst >> 16);
  sib->index = SIB_GET_INDEX(inst >> 16);
  sib->base = SIB_GET_BASE(inst >> 16);
}
