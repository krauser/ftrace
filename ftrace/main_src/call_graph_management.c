/*
** call_graph_management.c for  in /home/lauren_k/teck2/ftrace/ftrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Tue Jun 26 14:04:07 2012 jean-baptiste laurent
** Last update Sat Jun 30 22:09:07 2012 jean-baptiste laurent
*/

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "ftrace.h"

/*
** Test if two elem on
** Work like cmp fct :
**	0  : equal
**	-1 : less than
**	+1 : greater than
*/
int	is_branch_equal(void const *a, void const *b)
{
  t_branch const	*_a;
  t_branch const	*_b;

  _a = a;
  _b = b;
  if (a && b)
  {
    if (_a->opcode == _b->opcode
    	&& _a->op_addr == _b->op_addr
    	&& _a->dest_addr == _b->dest_addr
    	&& _a->next_inst_addr == _b->next_inst_addr)
      return (0);
  }
  return (-1);
}

/*
** Add an element on the graph
** If an elem already exist, just inc counter
*/
void		add_elem_on_graph(t_list **call_ret_list,
				  t_branch const *branch)
{
  t_branch	*current;

  if (call_ret_list && branch)
  {
    if ((current = (t_branch *)list_get_data(*call_ret_list,
    					     branch, &is_branch_equal)))
      current->occurence += 1;
    else
      list_push_back(call_ret_list, branch, sizeof(*branch));
  }
}
