/*
** call_management.c for  in /home/lauren_k/teck2/ftrace/ftrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Mon Jun 25 14:39:35 2012 jean-baptiste laurent
** Last update Sun Jul  1 14:26:16 2012 jean-baptiste laurent
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "ftrace.h"

int		is_call(t_data inst)
{
  char		modrm;

  modrm = (inst & 0xff00) >> 8;
  if (CALL_REL(inst))
    return (true);
  else if (CALL_MOD(inst) && test_fct_modrm(modrm))
    return (true);
  else if (CALL_PREF(inst) && test_fct_modrm((inst & 0xff0000) >> 16))
    return (true);
  return (false);
}

/*
** Return the size of an instruction with
**	-> (prefix not supported)
**	-> the mod r/m byte
**	-> sib
**	-> Displacement
*/
unsigned int	get_call_size(int inst)
{
  unsigned int	size;
  t_modrm	modrm;

  size = 0;
  if (IS_CALL(inst) && CALL_MOD(inst))
  {
    size = 2;
    fill_modrm_struct(inst, &modrm);
    size += GET_DISP_SIZE(modrm.mod);
    size += HAS_SIB(modrm.rm);
  }
  else if (IS_CALL(inst) && CALL_REL(inst))
    size = 5;
  return (size);
}

/*
** Parse call argument
** Return the address to wich the call is going
*/
static size_t	get_call_dest_addr(int op, int arg,
				   int pid, t_regtab const *reg)
{
  size_t	dest;

  dest = 0;
  if (CALL_REL(op))
  {
  }
  else if (CALL_MOD(op))
  {
    dest = decode_full_modrm(op, arg, pid, reg);
  }
  return (dest);
}

/*
** Fill the struct t_branch
** PAREIL QUE LE RET
*/
static void	_fill_call_branch(t_branch *call, t_data op,
				  t_regtab const *src)
{
  memset(call, 0, sizeof(*call));
  call->occurence = 1;
  call->opcode = (op & 0xff);
  call->op_addr = src->rip;
  call->next_inst_addr = src->rip + get_call_size(op);
}

void		aff_call(t_ftrace *info, int pid,
			 t_regtab const *reg_before,
			 t_regtab const *reg_after)
{
  t_data	op;
  int		arg;
  t_branch	call;

  (void)info;
  if (info && reg_before && reg_after)
  {
    op = lt_getdata(pid, (void *)(reg_before->rip));
    if (is_call(op))
    {
      if (CALL_PREF(op))
	op = op >> 8;
      arg = lt_getdata(pid, (void *)(reg_before->rip + sizeof(int)));
      _fill_call_branch(&call, op, reg_before);
      call.dest_addr = get_call_dest_addr(op, arg, pid, reg_before);
      call.dest_addr = reg_after->rip;
      if (call.dest_addr &&call.dest_addr != reg_after->rip)
      	fprintf(stderr, "Modrm decode error [%#lx][%#lx]\n",
      		(long)call.dest_addr, (long)reg_after->rip);
      add_elem_on_graph(&info->call_ret_list, &call);
    }
  }
  (void)pid;
  (void)reg_before;
  (void)reg_after;
}
