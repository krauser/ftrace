/*
** pid_management.c for  in /home/lauren_k/teck2/strace/strace-2015-2014s-lauren_k
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun May 13 22:28:56 2012 jean-baptiste laurent
** Last update Sun Jul  1 19:05:54 2012 jean-baptiste laurent
*/

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include "ftrace.h"

static int	pid = 0;

/*
** Attach the proc on option '-p'
*/
int		attach_proc(t_opt *opt)
{
  if (opt && S_PID(opt->opt))
  {
    if (lt_attach(opt->pid))
    {
      pid = opt->pid;
      fprintf(stderr, "Process %d attached\n", opt->pid);
      return (opt->pid);
    }
    else
      perror("strace ATTACH");
    return (-1);
  }
  return (0);
}

/*
** Attach the proc on option '-p'
*/
int		detach_proc(void)
{
  if (lt_detach(pid))
  {
    fprintf(stderr, "Process %d detached\n", pid);
    return (pid);
  }
  else
    perror("strace DETTACH");
  return (-1);
}
