/*
** modrm_management.c for  in /home/lauren_k/teck2/ftrace/ftrace
**
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
**
** Started on  Mon Jun 25 14:38:57 2012 jean-baptiste laurent
** Last update Sun Jul  1 19:04:05 2012 jean-baptiste laurent
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>
#include "ftrace.h"

/* Prefix */
/*   7                           0 */
/* +---+---+---+---+---+---+---+---+ */
/* | 0   1   0   0 | w | r | x | b | */
/* +---+---+---+---+---+---+---+---+ */
/*   * b0100 (4 bits) */
/*   * REX.w (1 bit) */
/*   * REX.r (1 bit) */
/*   * REX.x (1 bit) */
/*   * REX.b (1 bit)  */
static ssize_t	_decode_full_sib(t_sib const *sib, ssize_t disp,
				 int pid, t_regtab const *reg)
{
  unsigned int	i;
  size_t	scale;
  size_t	ret;

  i = -1;
  ret = 0;
  scale = 1;
  while (++i < sib->ss)
    scale = scale * 2;
  if (sib->index != 4)
  {
#ifdef FTRACE_ARCH_I386
    ret = (gREG(reg, sib->index) * scale + gREG(reg, sib->base)) + disp;
#else
    ret = (gREG_64(reg, sib->index) * scale + gREG(reg, sib->base)) + disp;
    ret = (gREG(reg, sib->index) * scale + gREG(reg, sib->base)) + disp;
#endif
    ret = lt_getdata(pid, (void *)(ret));
  }
  else
    ret = lt_getdata(pid, (void *)(gREG(reg, sib->base) + disp));
  errno = 0;
  return (ret);
}

static ssize_t	_adjust_disp(ssize_t ret, t_modrm const *modrm)
{
  if (GET_DISP_SIZE(modrm->mod) == 1)
    ret = (char)ret;
  else if (GET_DISP_SIZE(modrm->mod) == 4)
    ret = (int)ret;
  return (ret);
}

/*
** Decode the full mod rm and return the value decoded
**
** From: http://www.c-jump.com/CIS77/CPU/x86/lecture.html
** MOD R/M Addressing Mode
** === === ================================
**  00 000 [ eax ]
**  01 000 [ eax + disp8 ]               (1)
**  10 000 [ eax + disp32 ]
**  11 000 register  ( al / ax / eax )   (2)
**  00 001 [ ecx ]
**  01 001 [ ecx + disp8 ]
**  10 001 [ ecx + disp32 ]
**  11 001 register  ( cl / cx / ecx )
**  00 010 [ edx ]
**  01 010 [ edx + disp8 ]
**  10 010 [ edx + disp32 ]
**  11 010 register  ( dl / dx / edx )
**  00 011 [ ebx ]
**  01 011 [ ebx + disp8 ]
**  10 011 [ ebx + disp32 ]
**  11 011 register  ( bl / bx / ebx )
**  00 100 SIB  Mode                     (3)
**  01 100 SIB  +  disp8  Mode
**  10 100 SIB  +  disp32  Mode
**  11 100 register  ( ah / sp / esp )
**  00 101 32-bit Displacement-Only Mode (4)
**  01 101 [ ebp + disp8 ]
**  10 101 [ ebp + disp32 ]
**  11 101 register  ( ch / bp / ebp )
**  00 110 [ esi ]
**  01 110 [ esi + disp8 ]
**  10 110 [ esi + disp32 ]
**  11 110 register  ( dh / si / esi )
**  00 111 [ edi ]
**  01 111 [ edi + disp8 ]
**  10 111 [ edi + disp32 ]
**  11 111 register  ( bh / di / edi )
*/
size_t		decode_full_modrm(int inst, int inst_ext,
				  int pid, t_regtab const *reg)
{
  ssize_t	ret;
  t_modrm	modrm;
  t_sib		sib;
  char		compact[sizeof(int) * 2];

  memcpy(&compact[0], &inst, sizeof(int));
  memcpy(&compact[sizeof(int)], &inst_ext, sizeof(int));
  fill_modrm_struct(inst, &modrm);
  fill_sib_struct(inst, &sib);
  memcpy(&ret, &compact[2 + HAS_SIB(modrm.rm)], GET_DISP_SIZE(modrm.mod) % 5);
  ret = _adjust_disp(ret, &modrm);
  if (modrm.mod == 3)
    ret = gREG(reg, modrm.rm);
  else if (modrm.mod != 3 && modrm.rm != 4
      && !(modrm.mod == 0 && modrm.rm == 5))
    ret = lt_getdata(pid, (void *)((ret += gREG(reg, modrm.rm))));
  else
  {
    if ((modrm.mod == 0 || modrm.mod == 2) && modrm.rm == 4)
      memcpy(&ret, &compact[3], sizeof(int));
    ret = _decode_full_sib(&sib, (ret = (int)ret), pid, reg);
  }
  errno = 0;
  return (ret);
}

/*
** Test the mod r/m
** Return (true) if it is a call, false otherwise
*/
int		test_fct_modrm(char modrm_char)
{
  if (MODRM_GET_REG(modrm_char) == 2 || MODRM_GET_REG(modrm_char) == 3)
    return (true);
  return (false);
}
