/*
** ret_management.c for  in /home/lauren_k/teck2/ftrace/ftrace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Mon Jun 25 14:41:13 2012 jean-baptiste laurent
** Last update Sun Jul  1 19:06:26 2012 jean-baptiste laurent
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "ftrace.h"

int		is_ret(int inst)
{
  return (IS_RET(inst));
}

/*
** Parse call argument
** Return the address to wich the call is going
*/
size_t		get_ret_dest_addr(int op, int pid,
				   t_regtab const *reg_before,
				   t_regtab const *reg_after)
{
  (void)pid;
  (void)reg_before;
  if (RET_NO_ARG(op))
  {
  }
  return (reg_after->rip);
}

/*
** Fill the struct t_branch
** PAREIL QUE LE CALL
*/
static void	_fill_ret_branch(t_branch *call, int op,
				  t_regtab const *src, t_regtab const *dest)
{
  op = (op & 0xff);
  memset(call, 0, sizeof(*call));
  call->occurence = 1;
  call->opcode = op;
  call->op_addr = src->rip;
  call->dest_addr = dest->rip;
}

void		aff_ret(t_ftrace *info, int pid,
			t_regtab const *reg_before,
			t_regtab const *reg_after)
{
  long		op;
  t_branch	ret;

  (void)info;
  if (info && reg_before && reg_after)
  {
    op = lt_getdata(pid, (void *)(reg_before->rip));
    if (is_ret(op))
    {
      _fill_ret_branch(&ret, op, reg_before, reg_after);
      add_elem_on_graph(&info->call_ret_list, &ret);
    }
  }
  (void)pid;
  (void)reg_before;
  (void)reg_after;
}
