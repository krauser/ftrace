/*
** parser.c for  in /tmp
**
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
**
** Started on  Fri May 11 13:15:43 2012 jean-baptiste laurent
** Last update Sun Jul  1 19:05:18 2012 jean-baptiste laurent
*/

#ifdef __linux__
# define _POSIX_C_SOURCE 2
#endif /* !__linux__ */

#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include "ftrace.h"

/*
** manage the output on file
*/
static void	open_output(char const *path, char **file)
{
  int		fd;

  (void)file;
  fd = open(path, O_RDWR | O_CREAT, 0644);
  if (fd != -1 && dup2(fd, 1) == -1)
  {
    fprintf(stderr, "Unable to open file [%s]\n", path);
    exit(1);
  }
}

/*
** Parse the input and fill opt with the option
** Return (0) or (-1) if an error occured
*/
int	ftrace_opt_parser(t_opt *opt, int argc, char **argv)
{
  int	i;
  int	ret;
  char	flags[] = S_FLAGS;

  if (opt && argc > 0)
  {
    memset(opt, 0, sizeof(*opt));
    while ((ret = getopt(argc, argv, GETOPT_OPTSTRING)) != -1)
    {
      i = -1;
      while (flags[++i] && flags[i] != ret);
      if (S_TIME(opt->opt) < 3)
	opt->opt |= 1 << i;
      if (ret == 'o')
	open_output(optarg, &opt->file);
      else if (ret == 'p')
	opt->pid = strtol(optarg, (char **)NULL, 10);
      else if (ret == 'v')
	opt->opt |= 1 << i;
      else if (ret == 'f')
	opt->fct_name = optarg;
    }
  }
  opt->prog_name = (optind < argc ? argv[optind] : NULL);
  return (optind);
}
