include mk/variables.mk
include mk/variables_gnu.mk

$(NAME):	$(OBJDIR) $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(LDFLAGS)

all:		$(NAME)

$(OBJDIR)/%.o:	$(SRCDIR)/%.c
	$(CC) -o $@ $(CFLAGS) -c $<

$(OBJDIR)/%.o:	$(SRCDIR_LIBLIST)/%.c
	$(CC) -o $@ $(CFLAGS) -c $<

$(OBJDIR):
	$(MKDIR) $(OBJDIR)

clean:
	$(RM) $(OBJ)

fclean:		clean
	$(RM) $(NAME)

re:		fclean $(NAME)
