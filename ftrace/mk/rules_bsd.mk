.include "variables.mk"
.include "variables_bsd.mk"

$(NAME):	$(OBJDIR) $(OBJ)
	$(CC) -o $(NAME) $(OBJ) $(LDFLAGS)

.SUFFIXES:		.o .c

.for i in $(SOURCES)
$(OBJDIR)/${i:.c=.o}:	$(SRCDIR)/${i}
	$(CC) -o ${.TARGET} $(CFLAGS) -c $(SRCDIR)/${i}
.endfor

.for i in $(SOURCES_LIBLIST)
$(OBJDIR)/${i:.c=.o}:	$(SRCDIR_LIBLIST)/${i}
	$(CC) -o ${.TARGET} $(CFLAGS) -c $(SRCDIR_LIBLIST)/${i}
.endfor

$(OBJDIR):
	$(MKDIR) $(OBJDIR)
	$(MKDIR) $(OBJDIR)/liblist

clean:
	$(RM) $(OBJ)

fclean:		clean
	$(RM) $(NAME)

re:		clean $(NAME)
