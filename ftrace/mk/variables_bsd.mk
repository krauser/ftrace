SYSTEM		!= uname | tr '[:lower:]' '[:upper:]' | tr '-' '_'
ARCH		!= uname -m | tr '[:lower:]' '[:upper:]' | tr '-' '_'

.if $(SYSTEM) == "DARWIN"
CFLAGS		+= -arch i386 -arch x86_64
LDFLAGS		+= -framework Security -arch i386 -arch x86_64
.endif

SRC		 = $(SOURCES:S/^/$(SRCDIR)\//) \
		   $(SOURCES_LIBLIST:S/^/$(SRCDIR_LIBLIST)\//)
OBJ		 = $(SOURCES:S/^/$(OBJDIR)\//:S/.c/.o/) \
		   $(SOURCES_LIBLIST:S/^/$(OBJDIR)\//:S/.c/.o/)

CFLAGS		+= -DFTRACE_SYSTEM_$(SYSTEM) -I$(HDRDIR)

