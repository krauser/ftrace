SYSTEM		 = ${shell uname | tr '[:lower:]' '[:upper:]' | tr '-' '_'}
ARCH		 = ${shell uname -m | tr '[:lower:]' '[:upper:]' | tr '-' '_'}

ifeq "$(SYSTEM)" "DARWIN"
CFLAGS		+= -arch i386 -arch x86_64
LDFLAGS		+= -framework Security -arch i386 -arch x86_64
endif

SRC		 = ${addprefix $(SRCDIR)/,$(SOURCES)} \
		   ${addprefix $(SRCDIR_LIBLIST)/,$(SOURCES_LIBLIST)}
OBJ		 = ${addprefix $(OBJDIR)/,$(SOURCES:.c=.o)} \
		   ${addprefix $(OBJDIR)/,$(SOURCES_LIBLIST:.c=.o)}

CFLAGS		+= -DFTRACE_SYSTEM_$(SYSTEM) -I$(HDRDIR)
