NAME		 = ftrace

CC		 = gcc
CFLAGS		+= -Wall -Wextra -Werror -Wfatal-errors
CFLAGS		+= -I../libtrace/hdr -D_BSD_SOURCE -Iliblist
CFLAGS		+= -I../libsymbol/hdr
LDFLAGS		+= -L../libtrace -ltrace
LDFLAGS		+= -L../libsymbol -lsymbol
AR		 = ar rcs
RM		 = rm -f
MKDIR		 = mkdir -p
RUBY		 = ruby

SRCDIR		 = main_src
SRCDIR_LIBLIST	 = liblist
OBJDIR		 = build
HDRDIR		 = include

SOURCES		 = main.c \
		   ptrace_annexes.c \
		   strace_core.c \
		   ftrace_core.c \
		   modrm_management.c \
		   modrm_annexes.c \
		   call_management.c \
		   ret_management.c \
		   parser.c \
		   pid_management.c \
		   aff_syscall.c \
		   make_dotty_graph.c \
		   print_dotty_graph.c \
		   print_graph_statistique.c \
		   call_graph_management.c
SOURCES_LIBLIST	+= list_access.c \
		   list_adding.c \
		   list_suppression.c

