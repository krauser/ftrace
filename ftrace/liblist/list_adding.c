/*
** list_adding.c for  in /home/lauren_k/teck2/bomberman/bomberman-2015-manzan_g/Server/liblist
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Thu May 10 13:58:28 2012 jean-baptiste laurent
** Last update Sun Jul  1 19:00:45 2012 jean-baptiste laurent
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include "liblist.h"

/*
** Basic list management (static)
*/
static t_list	*_new_elem(void const *data, size_t size)
{
  t_list	*elem;

  if (data && size > 0)
  {
    errno = 0;
    elem = calloc(1, sizeof(*elem));
    if (!elem || !(elem->data = malloc(size)))
    {
      free(elem ? elem->data : NULL);
      free(elem);
    }
    else
    {
      errno = 0;
      memcpy(elem->data, data, size);
      return (elem);
    }
  }
  else
    fprintf(stderr, "liblist: Nothing to be done\n");
  return (NULL);
}

/*
** Add an element at the end
** Return a pointer to the last element
*/
t_list		*list_push_back(t_list **list, void const *data, size_t size)
{
  t_list	*new_elem;
  t_list	*last_elem;

  if (list && *list)
  {
    if ((new_elem = _new_elem(data, size)))
    {
      last_elem = list_last_elem(*list);
      last_elem->next = new_elem;
      last_elem->next->prev = last_elem;
      return (new_elem);
    }
  }
  else if (list)
  {
    *list = _new_elem(data, size);
    return (*list);
  }
  else
    fprintf(stderr, "list_push_back: list is null\n");
  return (NULL);
}

static t_list	*_add_in_middle(t_list *elem, t_list *new_elem, t_list **list)
{
  new_elem->next = elem;
  new_elem->prev = elem->prev;
  elem->prev->next = new_elem;
  elem->prev = new_elem;
  return (*list);
}

/*
** Add in the list sorted
** Add the elem if cmp return a negativ number (less than)
** Add before the item who match
*/
t_list		*list_push_sort(t_list **list, void const *ref, size_t size,
				int (*cmp)(void const *d1, void const *d2))
{
  t_list	*elem;
  t_list	*new_elem;
  t_list	*last_elem;
  t_list	*ret;

  ret = NULL;
  if (list && ref)
  {
    elem = *list;
    while (elem && (!elem->data || cmp(ref, elem->data) >= 0))
    {
      if (elem->next == NULL)
	last_elem = elem;
      elem = elem->next;
    }
    if (!*list || (elem && elem->prev == NULL))
      ret = list_push_front(list, ref, size);
    else if (elem && (new_elem = _new_elem(ref, size)))
      ret = _add_in_middle(elem, new_elem, list);
    else
      ret = list_push_back(&last_elem, ref, size);
  }
  return (ret);
}

/*
** Add an element at front
** Return the new list or (NULL) in error.
*/
t_list		*list_push_front(t_list **list, void const *data, size_t size)
{
  t_list	*new_elem;

  if (list && *list)
  {
    if ((new_elem = _new_elem(data, size)))
    {
      new_elem->next = *list;
      new_elem->next->prev = new_elem;
      *list = (*list)->prev;
      return (*list);
    }
  }
  else if (list)
  {
    *list = _new_elem(data, size);
    return (*list);
  }
  else
    fprintf(stderr, "list_push_front: list is NULL\n");
  return (NULL);
}
