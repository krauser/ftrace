/*
** list_access.c for  in /home/lauren_k/teck2/bomberman/bomberman-2015-manzan_g/Server/liblist
**
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
**
** Started on  Thu May 10 13:48:01 2012 jean-baptiste laurent
** Last update Sat Jun 23 14:37:19 2012 jean-baptiste laurent
*/

#include <unistd.h>
#include "liblist.h"

/*
** Return the last element on the list
*/
t_list	*list_last_elem(t_list *list)
{
  if (list)
  {
    while (list->next)
      list = list->next;
  }
  return (list);
}

/*
** Find an element on the list
** Return the list element
*/
t_list	*list_get_elem(t_list *list, void const *ref,
		       int (*cmp)(void const *, void const *))
{
  if (list)
  {
    while (list && cmp(ref, list->data))
      list = list->next;
  }
  return (list);
}

/*
** Find an element on the list
** Return a pointer to the data of in the struct
*/
void	*list_get_data(t_list *list, void const *ref,
		       int (*cmp)(void const *, void const *))
{
  if (list)
  {
    while (list && cmp(ref, list->data))
      list = list->next;
  }
  return (list ? list->data : NULL);
}

/*
** Return the data of the first elem
*/
void	*list_get_first(t_list *list)
{
  if (list)
    return (list->data);
  return (NULL);
}

/*
** Return the size of the list
*/
size_t		list_size(t_list const *list)
{
  size_t	size;

  size = 0;
  while (list)
  {
    list = list->next;
    ++size;
  }
  return (size);
}
