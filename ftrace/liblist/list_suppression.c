/*
** list_suppression.c for  in /home/lauren_k/teck2/bomberman/bomberman-2015-manzan_g/Server/liblist
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Thu May 10 14:18:21 2012 jean-baptiste laurent
** Last update Sun Jul  1 19:01:52 2012 jean-baptiste laurent
*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "liblist.h"

/*
** Erase the last element
*/
void		list_pop_back(t_list **list)
{
  t_list	*last_elem;

  if (list && *list && (*list)->next)
  {
    last_elem = list_last_elem(*list);
    last_elem->prev->next = NULL;
    free(last_elem->data);
    free(last_elem);
  }
  else if (list && *list && (*list)->next == NULL)
  {
    last_elem = *list;
    *list = (*list)->prev;
    if (*list)
      (*list)->next = NULL;
    free(last_elem->data);
    free(last_elem);
  }
  else
    fprintf(stderr, "list_pop_back: list already empty\n");
}

/*
** Erase the first element
*/
void		list_pop_front(t_list **list)
{
  t_list	*first_elem;

  if (list && *list)
  {
    first_elem = (*list);
    *list = (*list)->next;
    if (*list)
      (*list)->prev = NULL;
    free(first_elem->data);
    free(first_elem);
  }
  else
    fprintf(stderr, "list_pop_front: list already empty\n");
}

static void	_del_in_middle(t_list *elem)
{
  elem->prev->next = elem->next;
  elem->next->prev = elem->prev;
  free(elem->data);
  free(elem);
}

/*
** Find an element on the list and erase it
*/
void		list_del_elem(t_list **list)
{
  t_list	*elem;

  if (list && *list)
  {
    elem = *list;
    if (elem)
    {
      if (elem->next == NULL)
	list_pop_back(list);
      else if (elem->prev == NULL)
      	list_pop_front(list);
      else
      {
	*list = (*list)->prev;
	_del_in_middle(elem);
      }
      return ;
    }
  }
}

/*
** Free the entire list
*/
void		free_list(t_list **list)
{
  while (list && *list)
    list_pop_front(list);
}
