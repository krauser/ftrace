/*
** liblist.h for \ in /home/lauren_k/teck2/bomberman/bomberman-2015-manzan_g/Server/liblist
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Thu May 10 13:53:35 2012 jean-baptiste laurent
** Last update Sun Jul  1 19:00:33 2012 jean-baptiste laurent
*/

#ifndef LIBLIST_H_
# define LIBLIST_H_

/*
** Simple list struct
*/
typedef struct	s_list
{
  void		*data;
  struct s_list	*next;
  struct s_list	*prev;
}		t_list;

/*
** List access
*/
t_list	*list_last_elem(t_list *list);
t_list	*list_get_elem(t_list *list, void const *ref,
		       int (*cmp)(void const *, void const *));
void	*list_get_data(t_list *list, void const *ref,
		       int (*cmp)(void const *, void const *));
void	*list_get_first(t_list *list);
size_t	list_size(t_list const *list);

/*
** List adding
*/
t_list	*list_push_back(t_list **list, void const *, size_t size);
t_list	*list_push_front(t_list **list, void const *, size_t size);
t_list	*list_push_sort(t_list **list, void const *, size_t size,
			int (*cmp)(void const *, void const *));

/*
** List erasing
*/
void	list_pop_back(t_list **list);
void	list_pop_front(t_list **list);
void	list_del_elem(t_list **list);
void	free_list(t_list **list);

#endif /* LIBLIST_H_ */
