/*
** main.c for  in /home/lauren_k/teck2/bomberman/bomberman-2015-manzan_g/Server/liblist
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Thu May 10 17:21:21 2012 jean-baptiste laurent
** Last update Thu May 10 18:04:26 2012 jean-baptiste laurent
*/

#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "liblist.h"

int	compare(void const *a, void const *b)
{
  return (memcmp(a, b, 4));
}

int	main()
{
  int	i;
  int	tmp;
  int	tmp1;
  t_list *list;

  list = NULL;
  list_last_elem(NULL);
  list_get_elem(NULL, NULL, NULL);
  list_get_data(NULL, NULL, NULL);
  list_get_first(NULL);
  list_size(NULL);
  list_push_back(NULL, NULL, 0);
  list_push_front(NULL, NULL, 0);
  list_pop_back(NULL);
  list_pop_front(NULL);
  list_del_elem(NULL, NULL, NULL);
  free_list(NULL);

/************/

  for (i = 0 ; i < 100 ; ++i)
  {
    list_push_back(&list, "0", sizeof("0"));
    list_pop_back(&list);
  }
  for (i = 0 ; i < 100 ; ++i)
  {
    list_push_back(&list, "0", sizeof("0"));
    list_pop_front(&list);
  }
  for ( i = 0 ; i < 100 ; ++i)
  {
    list_push_back(&list, "0", sizeof("0"));
    list_push_back(&list, "0", sizeof("0"));
    list_push_back(&list, "0", sizeof("0"));
    list_pop_front(&list);
    list_pop_back(&list);
  }
  for (i = 0 ; i < 100 ; ++i)
  {
    list_push_back(&list, "0", sizeof("0"));
    list_push_back(&list, "0", sizeof("0"));
    list_push_back(&list, "0", sizeof("0"));
    list_pop_front(&list);
    list_pop_back(&list);
    list_push_back(&list, "0", sizeof("0"));
    free_list(&list);
  }
  for (i = 0 ; i < 1000 ; ++i)
    list_push_back(&list, "0", sizeof("0"));
  free_list(&list);

  srandom(time(NULL));
  for (i = 0 ; i < 1000000 ; ++i)
  {
    tmp = random() % 11;
    tmp1 = random() % 11;
    /* printf("Size = %ld\n", list_size(list)); */
    switch (tmp)
    {
      case 0: list_last_elem(list); break;
      case 1: list_get_elem(list, &tmp1, &compare); break;
      case 2: list_get_data(list, &tmp1, &compare); break;
      case 3: list_get_first(list); break;
      case 4: list_size(list); break;
      case 5: list_push_back(&list,&tmp1, 4); break;
      case 6: list_push_front(&list,&tmp1, 4); break;
      case 7: list_pop_back(&list); break;
      case 8: list_pop_front(&list); break;
      case 9: list_del_elem(&list, &tmp1, &compare); break;
      case 10: free_list(&list); break;
    }
  }
  free_list(&list);

  return (0);
}
