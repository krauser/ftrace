/*
** strace_gen_tab.sh for  in /home/lauren_k/teck2/strace
** 
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
** 
** Started on  Sun May  6 22:49:10 2012 jean-baptiste laurent
** Last update Sun May  6 22:49:11 2012 jean-baptiste laurent
*/

#ifndef ERRNO_MSG_H_
# define ERRNO_MSG_H_

# include <errno.h>

t_errno_msg const	errno_tab[] =
{
#ifdef E2BIG
     {E2BIG, "E2BIG", "Argument list too long "},
#endif /* !E2BIG */
#ifdef EACCES
     {EACCES, "EACCES", "Permission denied "},
#endif /* !EACCES */
#ifdef EADDRINUSE
     {EADDRINUSE, "EADDRINUSE", "Address already in use "},
#endif /* !EADDRINUSE */
#ifdef EADDRNOTAVAIL
     {EADDRNOTAVAIL, "EADDRNOTAVAIL", "Address not available "},
#endif /* !EADDRNOTAVAIL */
#ifdef EAFNOSUPPORT
     {EAFNOSUPPORT, "EAFNOSUPPORT", "Address family not supported "},
#endif /* !EAFNOSUPPORT */
#ifdef EAGAIN
     {EAGAIN, "EAGAIN", "Resource temporarily unavailable "},
#endif /* !EAGAIN */
#ifdef EALREADY
     {EALREADY, "EALREADY", "Connection already in progress "},
#endif /* !EALREADY */
#ifdef EBADE
     {EBADE, "EBADE", "Invalid exchange"},
#endif /* !EBADE */
#ifdef EBADF
     {EBADF, "EBADF", "Bad file descriptor "},
#endif /* !EBADF */
#ifdef EBADFD
     {EBADFD, "EBADFD", "File descriptor in bad state"},
#endif /* !EBADFD */
#ifdef EBADMSG
     {EBADMSG, "EBADMSG", "Bad message "},
#endif /* !EBADMSG */
#ifdef EBADR
     {EBADR, "EBADR", "Invalid request descriptor"},
#endif /* !EBADR */
#ifdef EBADRQC
     {EBADRQC, "EBADRQC", "Invalid request code"},
#endif /* !EBADRQC */
#ifdef EBADSLT
     {EBADSLT, "EBADSLT", "Invalid slot"},
#endif /* !EBADSLT */
#ifdef EBUSY
     {EBUSY, "EBUSY", "Device or resource busy "},
#endif /* !EBUSY */
#ifdef ECANCELED
     {ECANCELED, "ECANCELED", "Operation canceled "},
#endif /* !ECANCELED */
#ifdef ECHILD
     {ECHILD, "ECHILD", "No child processes "},
#endif /* !ECHILD */
#ifdef ECHRNG
     {ECHRNG, "ECHRNG", "Channel number out of range"},
#endif /* !ECHRNG */
#ifdef ECOMM
     {ECOMM, "ECOMM", "Communication error on send"},
#endif /* !ECOMM */
#ifdef ECONNABORTED
     {ECONNABORTED, "ECONNABORTED", "Connection aborted "},
#endif /* !ECONNABORTED */
#ifdef ECONNREFUSED
     {ECONNREFUSED, "ECONNREFUSED", "Connection refused "},
#endif /* !ECONNREFUSED */
#ifdef ECONNRESET
     {ECONNRESET, "ECONNRESET", "Connection reset "},
#endif /* !ECONNRESET */
#ifdef EDEADLK
     {EDEADLK, "EDEADLK", "Resource deadlock avoided "},
#endif /* !EDEADLK */
#ifdef EDEADLOCK
     {EDEADLOCK, "EDEADLOCK", "Synonym for EDEADLK"},
#endif /* !EDEADLOCK */
#ifdef EDESTADDRREQ
     {EDESTADDRREQ, "EDESTADDRREQ", "Destination address required "},
#endif /* !EDESTADDRREQ */
#ifdef EDOM
     {EDOM, "EDOM", "Mathematics argument out of domain of function "},
#endif /* !EDOM */
#ifdef EDQUOT
     {EDQUOT, "EDQUOT", "Disk quota exceeded "},
#endif /* !EDQUOT */
#ifdef EEXIST
     {EEXIST, "EEXIST", "File exists "},
#endif /* !EEXIST */
#ifdef EFAULT
     {EFAULT, "EFAULT", "Bad address "},
#endif /* !EFAULT */
#ifdef EFBIG
     {EFBIG, "EFBIG", "File too large "},
#endif /* !EFBIG */
#ifdef EHOSTDOWN
     {EHOSTDOWN, "EHOSTDOWN", "Host is down"},
#endif /* !EHOSTDOWN */
#ifdef EHOSTUNREACH
     {EHOSTUNREACH, "EHOSTUNREACH", "Host is unreachable "},
#endif /* !EHOSTUNREACH */
#ifdef EIDRM
     {EIDRM, "EIDRM", "Identifier removed "},
#endif /* !EIDRM */
#ifdef EILSEQ
     {EILSEQ, "EILSEQ", "Illegal byte sequence "},
#endif /* !EILSEQ */
#ifdef EINPROGRESS
     {EINPROGRESS, "EINPROGRESS", "Operation in progress "},
#endif /* !EINPROGRESS */
#ifdef EINTR
     {EINTR, "EINTR", "Interrupted function call "},
#endif /* !EINTR */
#ifdef EINVAL
     {EINVAL, "EINVAL", "Invalid argument "},
#endif /* !EINVAL */
#ifdef EIO
     {EIO, "EIO", "Input/output error "},
#endif /* !EIO */
#ifdef EISCONN
     {EISCONN, "EISCONN", "Socket is connected "},
#endif /* !EISCONN */
#ifdef EISDIR
     {EISDIR, "EISDIR", "Is a directory "},
#endif /* !EISDIR */
#ifdef EISNAM
     {EISNAM, "EISNAM", "Is a named type file"},
#endif /* !EISNAM */
#ifdef EKEYEXPIRED
     {EKEYEXPIRED, "EKEYEXPIRED", "Key has expired"},
#endif /* !EKEYEXPIRED */
#ifdef EKEYREJECTED
     {EKEYREJECTED, "EKEYREJECTED", "Key was rejected by service"},
#endif /* !EKEYREJECTED */
#ifdef EKEYREVOKED
     {EKEYREVOKED, "EKEYREVOKED", "Key has been revoked"},
#endif /* !EKEYREVOKED */
#ifdef EL2HLT
     {EL2HLT, "EL2HLT", "Level 2 halted"},
#endif /* !EL2HLT */
#ifdef EL2NSYNC
     {EL2NSYNC, "EL2NSYNC", "Level 2 not synchronized"},
#endif /* !EL2NSYNC */
#ifdef EL3HLT
     {EL3HLT, "EL3HLT", "Level 3 halted"},
#endif /* !EL3HLT */
#ifdef EL3RST
     {EL3RST, "EL3RST", "Level 3 halted"},
#endif /* !EL3RST */
#ifdef ELIBACC
     {ELIBACC, "ELIBACC", "Cannot access a needed shared library"},
#endif /* !ELIBACC */
#ifdef ELIBBAD
     {ELIBBAD, "ELIBBAD", "Accessing a corrupted shared library"},
#endif /* !ELIBBAD */
#ifdef ELIBMAX
     {ELIBMAX, "ELIBMAX", "Attempting to link in too many shared libraries"},
#endif /* !ELIBMAX */
#ifdef ELIBSCN
     {ELIBSCN, "ELIBSCN", "lib section in a"},
#endif /* !ELIBSCN */
#ifdef ELIBEXEC
     {ELIBEXEC, "ELIBEXEC", "Cannot exec a shared library directly"},
#endif /* !ELIBEXEC */
#ifdef ELOOP
     {ELOOP, "ELOOP", "Too many levels of symbolic links "},
#endif /* !ELOOP */
#ifdef EMEDIUMTYPE
     {EMEDIUMTYPE, "EMEDIUMTYPE", "Wrong medium type"},
#endif /* !EMEDIUMTYPE */
#ifdef EMFILE
     {EMFILE, "EMFILE", "Too many open files "},
#endif /* !EMFILE */
#ifdef EMLINK
     {EMLINK, "EMLINK", "Too many links "},
#endif /* !EMLINK */
#ifdef EMSGSIZE
     {EMSGSIZE, "EMSGSIZE", "Message too long "},
#endif /* !EMSGSIZE */
#ifdef EMULTIHOP
     {EMULTIHOP, "EMULTIHOP", "Multihop attempted "},
#endif /* !EMULTIHOP */
#ifdef ENAMETOOLONG
     {ENAMETOOLONG, "ENAMETOOLONG", "Filename too long "},
#endif /* !ENAMETOOLONG */
#ifdef ENETDOWN
     {ENETDOWN, "ENETDOWN", "Network is down "},
#endif /* !ENETDOWN */
#ifdef ENETRESET
     {ENETRESET, "ENETRESET", "Connection aborted by network "},
#endif /* !ENETRESET */
#ifdef ENETUNREACH
     {ENETUNREACH, "ENETUNREACH", "Network unreachable "},
#endif /* !ENETUNREACH */
#ifdef ENFILE
     {ENFILE, "ENFILE", "Too many open files in system "},
#endif /* !ENFILE */
#ifdef ENOBUFS
     {ENOBUFS, "ENOBUFS", "No buffer space available "},
#endif /* !ENOBUFS */
#ifdef ENODATA
     {ENODATA, "ENODATA", "No message is available on the STREAM head read que\
ue "},
#endif /* !ENODATA */
#ifdef ENODEV
     {ENODEV, "ENODEV", "No such device "},
#endif /* !ENODEV */
#ifdef ENOENT
     {ENOENT, "ENOENT", "No such file or directory "},
#endif /* !ENOENT */
#ifdef ENOEXEC
     {ENOEXEC, "ENOEXEC", "Exec format error "},
#endif /* !ENOEXEC */
#ifdef ENOKEY
     {ENOKEY, "ENOKEY", "Required key not available"},
#endif /* !ENOKEY */
#ifdef ENOLCK
     {ENOLCK, "ENOLCK", "No locks available "},
#endif /* !ENOLCK */
#ifdef ENOLINK
     {ENOLINK, "ENOLINK", "Link has been severed "},
#endif /* !ENOLINK */
#ifdef ENOMEDIUM
     {ENOMEDIUM, "ENOMEDIUM", "No medium found"},
#endif /* !ENOMEDIUM */
#ifdef ENOMEM
     {ENOMEM, "ENOMEM", "Not enough space "},
#endif /* !ENOMEM */
#ifdef ENOMSG
     {ENOMSG, "ENOMSG", "No message of the desired type "},
#endif /* !ENOMSG */
#ifdef ENONET
     {ENONET, "ENONET", "Machine is not on the network"},
#endif /* !ENONET */
#ifdef ENOPKG
     {ENOPKG, "ENOPKG", "Package not installed"},
#endif /* !ENOPKG */
#ifdef ENOPROTOOPT
     {ENOPROTOOPT, "ENOPROTOOPT", "Protocol not available "},
#endif /* !ENOPROTOOPT */
#ifdef ENOSPC
     {ENOSPC, "ENOSPC", "No space left on device "},
#endif /* !ENOSPC */
#ifdef ENOSR
     {ENOSR, "ENOSR", "No STREAM resources "},
#endif /* !ENOSR */
#ifdef ENOSTR
     {ENOSTR, "ENOSTR", "Not a STREAM "},
#endif /* !ENOSTR */
#ifdef ENOSYS
     {ENOSYS, "ENOSYS", "Function not implemented "},
#endif /* !ENOSYS */
#ifdef ENOTBLK
     {ENOTBLK, "ENOTBLK", "Block device required"},
#endif /* !ENOTBLK */
#ifdef ENOTCONN
     {ENOTCONN, "ENOTCONN", "The socket is not connected "},
#endif /* !ENOTCONN */
#ifdef ENOTDIR
     {ENOTDIR, "ENOTDIR", "Not a directory "},
#endif /* !ENOTDIR */
#ifdef ENOTEMPTY
     {ENOTEMPTY, "ENOTEMPTY", "Directory not empty "},
#endif /* !ENOTEMPTY */
#ifdef ENOTSOCK
     {ENOTSOCK, "ENOTSOCK", "Not a socket "},
#endif /* !ENOTSOCK */
#ifdef ENOTSUP
     {ENOTSUP, "ENOTSUP", "Operation not supported "},
#endif /* !ENOTSUP */
#ifdef ENOTTY
     {ENOTTY, "ENOTTY", "Inappropriate I/O control operation "},
#endif /* !ENOTTY */
#ifdef ENOTUNIQ
     {ENOTUNIQ, "ENOTUNIQ", "Name not unique on network"},
#endif /* !ENOTUNIQ */
#ifdef ENXIO
     {ENXIO, "ENXIO", "No such device or address "},
#endif /* !ENXIO */
#ifdef EOPNOTSUPP
     {EOPNOTSUPP, "EOPNOTSUPP", "Operation not supported on socket "},
#endif /* !EOPNOTSUPP */
#ifdef EOVERFLOW
     {EOVERFLOW, "EOVERFLOW", "Value too large to be stored in data type "},
#endif /* !EOVERFLOW */
#ifdef EPERM
     {EPERM, "EPERM", "Operation not permitted "},
#endif /* !EPERM */
#ifdef EPFNOSUPPORT
     {EPFNOSUPPORT, "EPFNOSUPPORT", "Protocol family not supported"},
#endif /* !EPFNOSUPPORT */
#ifdef EPIPE
     {EPIPE, "EPIPE", "Broken pipe "},
#endif /* !EPIPE */
#ifdef EPROTO
     {EPROTO, "EPROTO", "Protocol error "},
#endif /* !EPROTO */
#ifdef EPROTONOSUPPORT
     {EPROTONOSUPPORT, "EPROTONOSUPPORT", "Protocol not supported "},
#endif /* !EPROTONOSUPPORT */
#ifdef EPROTOTYPE
     {EPROTOTYPE, "EPROTOTYPE", "Protocol wrong type for socket "},
#endif /* !EPROTOTYPE */
#ifdef ERANGE
     {ERANGE, "ERANGE", "Result too large "},
#endif /* !ERANGE */
#ifdef EREMCHG
     {EREMCHG, "EREMCHG", "Remote address changed"},
#endif /* !EREMCHG */
#ifdef EREMOTE
     {EREMOTE, "EREMOTE", "Object is remote"},
#endif /* !EREMOTE */
#ifdef EREMOTEIO
     {EREMOTEIO, "EREMOTEIO", "Remote I/O error"},
#endif /* !EREMOTEIO */
#ifdef ERESTART
     {ERESTART, "ERESTART", "Interrupted system call should be restarted"},
#endif /* !ERESTART */
#ifdef EROFS
     {EROFS, "EROFS", "Read-only file system "},
#endif /* !EROFS */
#ifdef ESHUTDOWN
     {ESHUTDOWN, "ESHUTDOWN", "Cannot send after transport endpoint shutdown"}\
,
#endif /* !ESHUTDOWN */
#ifdef ESPIPE
     {ESPIPE, "ESPIPE", "Invalid seek "},
#endif /* !ESPIPE */
#ifdef ESOCKTNOSUPPORT
     {ESOCKTNOSUPPORT, "ESOCKTNOSUPPORT", "Socket type not supported"},
#endif /* !ESOCKTNOSUPPORT */
#ifdef ESRCH
     {ESRCH, "ESRCH", "No such process "},
#endif /* !ESRCH */
#ifdef ESTALE
     {ESTALE, "ESTALE", "Stale file handle "},
#endif /* !ESTALE */
#ifdef ESTRPIPE
     {ESTRPIPE, "ESTRPIPE", "Streams pipe error"},
#endif /* !ESTRPIPE */
#ifdef ETIME
     {ETIME, "ETIME", "Timer expired "},
#endif /* !ETIME */
#ifdef ETIMEDOUT
     {ETIMEDOUT, "ETIMEDOUT", "Connection timed out "},
#endif /* !ETIMEDOUT */
#ifdef ETXTBSY
     {ETXTBSY, "ETXTBSY", "Text file busy "},
#endif /* !ETXTBSY */
#ifdef EUCLEAN
     {EUCLEAN, "EUCLEAN", "Structure needs cleaning"},
#endif /* !EUCLEAN */
#ifdef EUNATCH
     {EUNATCH, "EUNATCH", "Protocol driver not attached"},
#endif /* !EUNATCH */
#ifdef EUSERS
     {EUSERS, "EUSERS", "Too many users"},
#endif /* !EUSERS */
#ifdef EWOULDBLOCK
     {EWOULDBLOCK, "EWOULDBLOCK", "Operation would block "},
#endif /* !EWOULDBLOCK */
#ifdef EXDEV
     {EXDEV, "EXDEV", "Improper link "},
#endif /* !EXDEV */
#ifdef EXFULL
     {EXFULL, "EXFULL", "Exchange full"},
#endif /* !EXFULL */
};

#endif /* !ERRNO_MSG_H_*/
