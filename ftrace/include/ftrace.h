/*
** strace.h for  in /home/lauren_k/teck2/strace
**
** Made by jean-baptiste laurent
** Login   <lauren_k@epitech.net>
**
** Started on  Wed May  2 22:29:20 2012 jean-baptiste laurent
** Last update Sun Jul  1 19:15:28 2012 jean-baptiste laurent
*/

#ifndef FTRACE_H_
# define FTRACE_H_

# include <unistd.h>
# include <sys/types.h>
# include <sys/ptrace.h>
# if defined(FTRACE_SYSTEM_DARWIN)
#  include <i386/endian.h>
# elif defined(FTRACE_SYSTEM_LINUX)
#  include <endian.h>
# else
#  include <sys/endian.h>
# endif
# include "libtrace.h"
# include "liblist.h"
# include "libsymbol.h"

# if defined(__amd64__)
#  define FTRACE_ARCH_AMD64
# elif defined(__i386__)
#  define FTRACE_ARCH_I386
# endif

# define USAGE			"Usage: ptrace [-p pid] | program"

# define DO_PRAGMA(x) _Pragma (#x)
# define TODO(x) DO_PRAGMA(message ("TODO - " #x))

/*
** Test du syscall
*/
# define IS_SYSCALL_L(x)	((x & 0xffff) == 0x050f)
# define IS_SYSENTER_L(x)	((x & 0xffff) == 0x350f)
# define IS_INT_L(x)		((x & 0xffff) == 0x80cd)

# define IS_SYSCALL_B(x)	((x & 0xffff) == 0x0f05)
# define IS_SYSENTER_B(x)	((x & 0xffff) == 0x0f35)
# define IS_INT_B(x)		((x & 0xffff) == 0xcd80)

# define IS_SYS_L(x)	(IS_SYSCALL_L(x) || IS_SYSENTER_L(x) || IS_INT_L(x))
# define IS_SYS_B(x)	(IS_SYSCALL_B(x) || IS_SYSENTER_B(x) || IS_INT_B(x))

/*
** Test du call / ret
*/
# define CALL_REL(x)	((x & 0xff) == 0xe8)
# define CALL_MOD(x)	((x & 0xff) == 0xff)
# define CALL_PREF(x)	((x & 0xf0) == 0x40 && CALL_MOD((x & 0xff00) >> 8))

# define RET_NO_ARG(x)	((x & 0xff) == 0xC3 || (x & 0xff) == 0xCB)
# define RET_ARG(x)	((x & 0xff) == 0xC2 || (x & 0xff) == 0xCA)

# define IS_CALL(x)	(CALL_REL(x) || CALL_MOD(x) || CALL_PREF(x))
# define IS_RET(x)	(RET_NO_ARG(x) || RET_ARG(x))

/*
** Maximum size for a string to be printed
*/
# define XSTR_CONVERT(s) STR_CONVERT(s)
# define STR_CONVERT(s) #s

# define CHAR_PTR_BUFF		512
# define CHAR_PTR_BUFF_STR	XSTR_CONVERT(CHAR_PTR_BUFF)

# define S_EXIT_MSG		"+++ exited (status %d) +++\n"
# define S_KILLED_SIG_MSG	"+++ killed by %s +++\n"
# define S_KILLED_USIG_MSG	"+++ killed by signal %ld +++\n"
# define S_SIGCHLD_MSG		"+++ received SIGCHLD +++\n"
# define S_SIGCONT_MSG		"+++ continue by SIGCONT +++\n"
# define S_UNKNOWN_MSG		"+++ Unknown exit occured +++\n"

/*
** Parsing define
** S_FLAGS must be in the same order as S_ define
*/
# define S_FLAGS		"hiqopvt"
# define GETOPT_OPTSTRING	"+hiqo:p:tvf:"

# define S_USAGE(x)		((x) & (1 << 0))
# define S_RIP_PTR(x)		((x) & (1 << 1))
# define S_QUITE(x)		((x) & (1 << 2))
# define S_OUTPUT(x)		((x) & (1 << 3))
# define S_PID(x)		((x) & (1 << 4))
# define S_VERBOSE(x)		((x) & (1 << 5))
# define S_TIME(x)		(((x) & (3 << 6)) >> 6)

/*
** MOD R/M SIB define
*/
/* Mod r/m ((mod).. (reg)... (rm)...) */
# define iREG			(0)
# define iREG_DISP8		(1)
# define iREG_DISP32		(4)

# define MODRM_GET_MOD(x)	(((x) & 0xC0) >> 6) /* 0b11 000 000 */
# define MODRM_GET_REG(x)	(((x) & 0x38) >> 3) /* 0b00 111 000 */
# define MODRM_GET_RM(x)	(((x) & 0x07) >> 0) /* 0b00 000 111 */

/* Sib ((scale).. (index)... (base)...) */
# define SIB_GET_SCALE(x)	(((x) & 0xC0) >> 6) /* 0b11 000 000 */
# define SIB_GET_INDEX(x)	(((x) & 0x38) >> 3) /* 0b00 111 000 */
# define SIB_GET_BASE(x)	(((x) & 0x07) >> 0) /* 0b00 000 111 */

/* macro */
# define HAS_SIB(rm)		(((rm) == 0x04) ? (1) : (0)) /* 0b100 */
# define GET_DISP_SIZE(mod)	(((mod) == 0 || (mod) == 3) ? (iREG) :	\
				 (((mod) == 1) ? (iREG_DISP8) : (iREG_DISP32)))

/*
** Decode full modrm sib
*/
# define IS_U(x, rm)	(0x00)
# define IS_RDI(x, rm)	(rm == 7 ? ((t_regtab const*)x)->rdi : IS_U(x, rm))
# define IS_RSI(x, rm)	(rm == 6 ? ((t_regtab const*)x)->rsi : IS_RDI(x, rm))
# define IS_RBP(x, rm)	(rm == 5 ? ((t_regtab const*)x)->rbp : IS_RSI(x, rm))

# define IS_SIB(rm)	(rm == 4 ? (1) : (0))
# define IS_RSP(x, rm)	(rm == 4 ? ((t_regtab const*)x)->rsp : IS_RBP(x, rm))

# define IS_RBX(x, rm)	(rm == 3 ? ((t_regtab const*)x)->rbx : IS_RSP(x, rm))
# define IS_RDX(x, rm)	(rm == 2 ? ((t_regtab const*)x)->rdx : IS_RBX(x, rm))
# define IS_RCX(x, rm)	(rm == 1 ? ((t_regtab const*)x)->rcx : IS_RDX(x, rm))
# define IS_RAX(x, rm)	(rm == 0 ? ((t_regtab const*)x)->rax : IS_RCX(x, rm))
# define gREG(x, rm)	(IS_RAX(x, rm))

# ifdef FTRACE_ARCH_AMD64

# define IS_RU(x, rm)	(0x00)
# define IS_R15(x, rm)	(rm == 7 ? ((t_regtab const*)x)->r15 : IS_RU(x, rm))
# define IS_R14(x, rm)	(rm == 6 ? ((t_regtab const*)x)->r14 : IS_R15(x, rm))
# define IS_R13(x, rm)	(rm == 5 ? ((t_regtab const*)x)->r13 : IS_R14(x, rm))

# define IS_SIB(rm)	(rm == 4 ? (1) : (0))
# define IS_R12(x, rm)	(rm == 4 ? ((t_regtab const*)x)->r12 : IS_R13(x, rm))

# define IS_R11(x, rm)	(rm == 3 ? ((t_regtab const*)x)->r11 : IS_R12(x, rm))
# define IS_R10(x, rm)	(rm == 2 ? ((t_regtab const*)x)->r10 : IS_R11(x, rm))
# define IS_R9(x, rm)	(rm == 1 ? ((t_regtab const*)x)->r9 : IS_R10(x, rm))
# define IS_R8(x, rm)	(rm == 0 ? ((t_regtab const*)x)->r8 : IS_R9(x, rm))
# define gREG_64(x, rm)	(IS_R8(x, rm))

# endif /*! FTRACE_ARCH_I386 */

/*
** Struct used for mod r/m sib
** There are not used with cast or union. Order of member does not matter
*/
typedef struct	s_modrm
{
  unsigned char	rm;
  unsigned char	reg;
  unsigned char	mod;
}		t_modrm;
typedef struct	s_sib
{
  unsigned char	base;
  unsigned char	index;
  unsigned char	ss;
}		t_sib;

/*
** Structure used for option
*/
typedef struct	s_opt
{
  int		opt;
  int		pid;
  char		*file;
  char	const	*prog_name;
  char	const	*fct_name;
}		t_opt;

typedef struct	s_branch
{
  char		*name;
  unsigned int	occurence;
  int		opcode;
  size_t	src_addr;
  size_t	dest_addr;
  size_t	op_addr;
  size_t	next_inst_addr;
}		t_branch;

typedef struct	s_ftrace
{
  t_opt		opt;
  size_t	prog_start;
  t_list	*call_ret_list;
}		t_ftrace;

/*
** Structure used to resolv signal name
*/
typedef struct	s_signal_msg
{
  int		sig_id;
  char		*sig_def;
}		t_signal_msg;

/*
** Structure used for errno
*/
typedef struct	s_errno_msg
{
  int		err_id;
  char		*err_def;
  char		*err_desc;
}		t_errno_msg;

/*
**
*/
typedef struct	s_statistique
{
  unsigned int	call;
  unsigned int	called;
  unsigned int	syscall;
  unsigned int	ret;
  unsigned int	returned;
  size_t	fct_src;
  size_t	fct_dest;
}		t_st;
/*
** main fct
*/
void		catch_sig(int sig);
int		attach_proc(t_opt *opt);
int		detach_proc(void);

int		ftrace_opt_parser(t_opt *opt, int argc, char **argv);
void		ftrace_main_loop(char *name, char **argv, t_ftrace *opt);

/*
** Strace fct
*/
int		is_sys(int inst);
void		aff_syscall(t_ftrace *info, int pid,
			    t_regtab const *reg_before,
			    t_regtab const *reg_after);
/*
** Ftrace fct
*/

/*
** Call management
*/
int		is_call(t_data inst);
void		aff_call(t_ftrace *info, int pid,
			 t_regtab const *reg_before,
			 t_regtab const *reg_after);
/*
** Ret management
*/
int		is_ret(int inst);
size_t		get_ret_dest_addr(int op, int pid,
				   t_regtab const *reg_before,
				   t_regtab const *reg_after);
void		aff_ret(t_ftrace *info, int pid,
			t_regtab const *reg_before,
			t_regtab const *reg_after);

/*
** Modrm management
*/
unsigned int	get_call_size(int op);
int		test_fct_modrm(char);
size_t		decode_full_modrm(int op, int arg,
				  int pid, t_regtab const *reg);
void		fill_sib_struct(int inst, t_sib *sib);
void		fill_modrm_struct(int inst, t_modrm *modrm);

/*
** Callgraph management
*/
int		compare_addr(void const *a, void const *b);
void		add_elem_on_graph(t_list **call_ret_list,
				  t_branch const *branch);
int		is_branch_equal(void const *a, void const *b);

void		make_dotty_graph(t_list const *call_ref_list,
				 t_ftrace const *info,
				 t_symbol const *sym);
void		print_graph(t_list const *call_ref_list, t_list const *list);
void		print_link(t_list const *list, t_branch const *curr);
size_t		get_curr_addr(t_list const *list, size_t addr);
size_t		get_curr_addr_sym(t_symbol const *fct_list, void *addr);
void		print_statistique(t_list const *fct_list,
				  t_list const *call_ref_list,
				  t_symbol const *sym);
void		find_fct_name(size_t addr, t_symbol const *sym);

/*
** Ptrace fct
*/
int		get_regtab(int pid, t_regtab *reg);
int		go_to_next_inst(int pid);
size_t		fill_data_from_process(int pid, long addr,
				       void *ptr, size_t size);

# endif /* ! FTRACE_H_ */
